#!/usr/bin/env python3

# extracts strings from *.py and .ui files and
# generates a gettext .pot template.
# update every existing po file with new strings

import os
from glob import glob

from bubblemail.config import PACKAGE_NAME
GLADE_DIR = './data'
PYTHON_DIR = f'./{PACKAGE_NAME}'
PLUGINS_DIR = f'{GLADE_DIR}/plugins'
UI_DIR = f'{PYTHON_DIR}/ui'
POT_FOLDER = './po'
POT_FILE = os.path.join(POT_FOLDER, f'{PACKAGE_NAME}.pot')

def list_files(path, extension):
    file_list = filter(os.path.isfile, glob(os.path.join(path, '*')))
    return filter(lambda f: os.path.splitext(f)[1].lower() == f'.{extension}',
                  file_list)

def get_locale_map():
    with open('po/LINGUAS', 'r') as linguas_file:
        return filter(len, map(lambda l: l.strip(),
                               linguas_file.read().split()))

if not os.path.exists(POT_FOLDER):
    os.makedirs(POT_FOLDER)
elif os.path.exists(POT_FILE):
    os.remove(POT_FILE)

# Generate string headers of all glade files
for folder in GLADE_DIR, PLUGINS_DIR:
    for glade_file in list_files(folder, 'ui'):
        print(f'Generating string headers from glade file {glade_file}')
        os.system(f'intltool-extract --type=gettext/glade {glade_file}')

# Write template files
PY_FILES = str()
for folder in PYTHON_DIR, PLUGINS_DIR, UI_DIR:
    for py_file in list_files(folder, 'py'):
        print(f'Adding python source file {py_file}')
        PY_FILES += f'{py_file} '
if PY_FILES:
    os.system(f'xgettext {PY_FILES} {GLADE_DIR}/*.h {PLUGINS_DIR}/*.h '
              + f'--keyword=_ --keyword=N_ '
              + f'--from-code=UTF-8 --output={POT_FILE}')

# Update po files
for locale in get_locale_map():
    if f'{POT_FOLDER}/{locale}.po' not in list_files(POT_FOLDER, 'po'):
        os.system(f'msginit --no-translator --input={POT_FILE} '
                  + f'--output-file={POT_FOLDER}/{locale}.po --locale={locale}')
    else:
        print(f'Updating po file for locale {locale}')
        os.system(f'msgmerge --update --backup=none {POT_FOLDER}/{locale}.po '
                  + f'{POT_FILE}')

# Clean up
for folder in GLADE_DIR, PLUGINS_DIR:
    for h_file in list_files(folder, 'h'):
        print(f'Removing generated header file {h_file}')
        os.remove(h_file)
