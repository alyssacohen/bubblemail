# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import threading
import time
import logging
import gi
from copy import copy
# pylint: disable=C0413
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
from gi.repository import Gtk, GLib #, Gdk

from . import check_ready
from .accountdialog import AccountDialog
from .foldersmenu import FoldersMenu
from .plugindialog import PluginDialog
from ..config import Config as C, PACKAGE_NAME
from ..account import Account as A, RemoteAccount as RA, LocalAccount as LA
from ..remotemail import RemoteMail as RM
from ..utils import get_data_file
from ..dbusservice import CFG_SET_METHOD
from ..i18n import _
# pylint: enable=C0413

class RowWidget:
    def __init__(self, caller):
        self.caller = caller
        self.config = caller.config
        self.dbus_send = caller.dbus_send
        builder = Gtk.Builder()
        builder.set_translation_domain(PACKAGE_NAME)
        builder.add_from_file(get_data_file('listboxrow.ui'))
        self.widget = builder.get_object
        builder.connect_signals({
            'on_edit_clicked': self.on_edit_clicked,
            'on_delete_clicked': self.on_delete_clicked,
            'on_enable_changed': self.on_enable_changed,
            'on_undo': self.on_undo})

    def add_to(self, listbox_widget):
        listbox_widget.insert(self.widget('row_listbox'), len(listbox_widget))

    def set_state(self, state, init=True):
        if init:
            self.widget('swt_enable').set_active(state)
        for widget_name in 'lbl_name', 'lbl_desc', 'btn_edit':
            self.widget(widget_name).set_sensitive(state)
        if hasattr(self, 'account') and self.account[RA.TYPE] == RA.GOA:  # pylint: disable=no-member
            self.widget('btn_edit').set_sensitive(False)

    def on_delete_clicked(self, widget):
        pass

    def on_edit_clicked(self, widget):
        pass

    def on_enable_changed(self, widget, state):  # pylint: disable=W0613
        self.set_state(state, init=False)

    def on_undo(self, widget):
        pass

class AccountRow(RowWidget):
    def __init__(self, caller, account):
        super().__init__(caller)
        self.account = account
        self.account_backup = None
        self.undo_request = False
        self.name = self.account[RA.NAME]
        if self.name[0].islower():
            self.name = self.name.capitalize()
        self.widget('lbl_name').set_label(self.name)
        desc = _('{0} account with ').format(self.account[RA.TYPE].capitalize())
        if self.account.is_local(self.account):
            desc += _('{0} <i>{1}</i>').format(self.account[RA.BACKEND],
                                               self.account[LA.PATH])
        else:
            desc += _('{0} server <i>{1}</i>').format(self.account[RA.BACKEND],
                                                      self.account[RA.SERVER])
        if account[account.ERROR]:
            self.widget('error_ico').set_visible(True)
            desc = account[account.ERROR]
        self.widget('lbl_desc').set_label(desc)
        for button in 'btn_delete', 'btn_edit':
            self.widget(button).set_sensitive(account[RA.TYPE] == RA.INTERNAL)
        self.widget('btn_delete').show()
        if account[RA.BACKEND] in (RA.IMAP, LA.MAILDIR):
            folders_button = self.widget('btn_folders')
            status = self.account[LA.STATUS]
            available = status in map(str, (RM.SUCCESS, RM.UNSECURE))
            if account[RA.BACKEND] == LA.MAILDIR or available:
                folders_button.set_sensitive(True)
                folders_button.connect('clicked', self.on_folders_clicked)
                self.folder_menu = FoldersMenu(caller, folders_button,
                                               self.account)
            else:
                folders_button.set_sensitive(False)
            enabled_folders_count = len(account[RA.FOLDERS])
            if enabled_folders_count:
                self.widget('lbl_enabled_folders').set_label(
                    str(len(account[RA.FOLDERS])))
                self.widget('lbl_enabled_folders').show()
        self.set_state(int(self.account[RA.ENABLED]))
        self.add_to(caller.widget('accounts_lbox'))

    @check_ready
    def on_folders_clicked(self, unused_widget):
        self.folder_menu.show()

    @check_ready
    def on_enable_changed(self, widget, state):
        super().on_enable_changed(widget, state)
        self.account[RA.ENABLED] = str(int(state))
        self.account.save(commit=False)
        logging.debug('On enable changed: set_config')
        self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())

    @check_ready
    def on_edit_clicked(self, unused_widget):
        AccountDialog(self.caller, self.account)

    @check_ready
    def on_delete_clicked(self, widget):
        self.account_backup = copy(self.account)
        self.account.delete(commit=False, keep_credentials=False)
        self.caller.config_lock = True
        self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())
        self.widget('undo_lbl').set_label(
            _('Account {0} has been removed').format(self.name))
        self.widget('row_stk').set_visible_child_name('undo')

        def undo_handler():
            time.sleep(self.caller.UNDO_DELAY)
            self.caller.config_lock = False
            if self.undo_request:
                self.undo_request = False
                return
            GLib.idle_add(self.widget('row_listbox').destroy)

        threading.Thread(target=undo_handler).start()

    def on_undo(self, widget):
        if self.undo_request:
            logging.error('Undo request already set !!!!')
            return
        if not self.account_backup:
            logging.error('Undo with no backup account to restore !!!!')
            return
        self.account_backup.save(create=True, commit=False)
        self.caller.config_lock = False
        self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())
        self.widget('row_stk').set_visible_child_name('default')
        self.caller.ui_update()

class PluginRow(RowWidget):
    def __init__(self, caller, plugin_error, plugin_name, plugin):
        super().__init__(caller)
        self.plugin_error = plugin_name in plugin_error
        self.plugin_name = plugin_name
        self.plugin = plugin
        enabled_lst = caller.config.get_core(C.ENABLED_PLUGINS, array=True)
        self.order_lst = caller.config.get_core(C.PLUGINS_ORDER, array=True)
        self.index = self.order_lst.index(self.plugin_name)
        desc = '%s\n%s' % plugin.MANIFEST[1:3]
        self.widget('btn_folders').set_visible(False)
        if self.plugin_error:
            self.widget('swt_enable').set_sensitive(False)
            self.widget('btn_edit').set_sensitive(False)
            self.widget('error_ico').set_visible(True)
            desc = f'Plugin failed to start'
        self.widget('lbl_name').set_label(plugin.MANIFEST[0].capitalize())
        self.widget('lbl_desc').set_label(desc)
        self.set_state(plugin_name in enabled_lst)
        for widget_name in 'btn_up', 'btn_down':
            self.widget(widget_name).set_visible(True)
            self.widget(widget_name).connect('clicked', self.on_order_changed)
        self.set_sensitive()
        self.add_to(caller.widget('plugins_lbox'))

    def set_sensitive(self):
        self.widget('btn_up').set_sensitive(
            self.plugin_name != self.order_lst[0])
        self.widget('btn_down').set_sensitive(
            self.plugin_name != self.order_lst[-1])

    @check_ready
    def on_order_changed(self, widget):
        self.order_lst.remove(self.plugin_name)
        self.index += 1 if widget.get_name() == 'down' else -1
        self.order_lst.insert(self.index, self.plugin_name)
        self.config.set_core(C.PLUGINS_ORDER, self.order_lst)
        self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())
        self.caller.ui_update()

    @check_ready
    def on_enable_changed(self, widget, state):
        super().on_enable_changed(widget, state)
        logging.info(f'Setting {self.plugin.human_name} to {state}')
        self.widget('btn_edit').set_sensitive(state)
        dbus_method = 'EnablePlugin' if state else 'DisablePlugin'
        self.dbus_send(dbus_method, arg=self.plugin_name)

    @check_ready
    def on_edit_clicked(self, unused_widget):
        PluginDialog(self.caller, self.plugin)
