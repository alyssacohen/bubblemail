# Copyright 2011 Patrick Ulbrich <zulu99@gmx.net>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
def check_ready(func):
    """ Decorator disabling signals until UI is ready, checking self.ready
        property state
    """
    def wrapper(self, *args, **kwargs):
        if hasattr(self, 'ready') and not self.ready:
            return None
        for prop in 'caller', 'parent':
            if hasattr(self, prop) and not getattr(self, prop).ready:
                return None
        return func(self, *args, **kwargs)
    return wrapper
