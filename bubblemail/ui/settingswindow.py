# Copyright 2011 - 2016 Patrick Ulbrich <zulu99@gmx.net>
# Copyright 2011 Ralf Hersel <ralf.hersel@gmx.net>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import os
# import webbrowser
import logging
import gi
# pylint: disable=C0413, W1202
gi.require_version('Gtk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GLib', '2.0')
from gi.repository import Gdk, Gtk, GLib, GdkPixbuf
import dbus
from dbus.mainloop.glib import DBusGMainLoop

# from ..config import (PACKAGE_NAME, CACHE_DIR, APP_VERSION, APP_URL, APP_DESC,
#                       DBUS_BUS_NAME, DBUS_OBJ_PATH, DOWNLOAD_URL, Config as C)
from ..config import (PACKAGE_NAME, CACHE_DIR, APP_VERSION, APP_URL, APP_DESC,
                      DBUS_BUS_NAME, DBUS_OBJ_PATH, Config as C)
from ..i18n import _
from ..utils import (init_logging, set_log_level, get_data_file,
                     service_is_running)
from ..account import Account as A, RemoteAccount, LocalAccount
from ..plugin import PluginAgregator
from ..dbusservice import (CFG_UPDATE_SIG, CFG_SET_METHOD, REFRESH_METHOD,
                           PLGSTATE_GET_METHOD)
from . import check_ready
from .rowwidget import AccountRow, PluginRow
from .accountdialog import AccountDialog

DBusGMainLoop(set_as_default=True)


# pylint: disable=R0904, R0914
class SettingsWindow:
    UNDO_DELAY = 8

    def __init__(self):
        self.ready = False
        self.config = C()
        init_logging(loglevel=self.config.log_level)
        self.config_lock = False
        self.window_size = None
        self.new_account = None
        builder = Gtk.Builder()
        builder.set_translation_domain(PACKAGE_NAME)
        builder.add_from_file(get_data_file('settings.ui'))
        self.widget = builder.get_object
        builder.connect_signals({
            'on_add': self.on_add,
            'on_back': self.on_back,
            'on_refresh': self.on_refresh,
            'on_logshow': self.on_logshow,
            'on_aboutshow': self.on_aboutshow,
            'on_poll_changed': self.on_poll_changed,
            'on_loglevel_changed': self.on_loglevel_changed,
            'on_lbox_pages_row_activated': self.on_lbox_pages_row_activated,
            'on_window_deleted': self.on_window_deleted,
            'on_auth_warn_dlg_closed': self.on_auth_warn_dlg_closed,
            'on_service_error_dlg_close': self.on_window_deleted,
            'on_window_resize': self.on_window_resize
        })
        if not service_is_running():
            self.widget('service_error_dlg').show()
        else:
            self.ui_init()
        Gtk.main()

    def ui_init(self):
        dbus_loop = dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        bus = dbus.SessionBus(dbus_loop)
        self.dbus_proxy = None
        self.dbus_proxy = bus.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
        self.dbus_proxy.connect_to_signal(CFG_UPDATE_SIG, self.on_config_change)
        self.widget_id = lambda w: w.get_name().split('_')[-1]
        self.widget('main_stk').set_visible_child_name('accounts')
        self.window = self.widget('settings_wdw')
        self.window.set_title(_(f'Bubblemail settings'))
        self.window.set_icon_name(PACKAGE_NAME)
        provider = Gtk.CssProvider()
        provider.load_from_path(get_data_file('settings.css'))
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        self.window_size = self.config.get_list(C.CORE, C.WINDOW_SIZE)
        self.window_size = tuple(map(int, self.window_size))
        if 0 not in self.window_size:
            self.widget('settings_wdw').set_default_size(self.window_size[0],
                                                         self.window_size[1])
        self.window.show()
        self.ui_update()
        # if self.config.get_core(C.UPDATE_AVAILABLE) == '1':
        #     if self.widget('new_version_dlg').run() == Gtk.ResponseType.OK:
        #         webbrowser.open(DOWNLOAD_URL)
        #     self.widget('new_version_dlg').destroy()

    def ui_update(self):
        self.ready = False
        # Logging level
        self.widget('loglevel_swt').set_active(
            int(self.config.get_core(C.DEBUG)))
        # Poll interval
        for btn in self.widget('pollmenu_box'):
            if btn.get_name() == self.config.get_core(C.POLL_INTERVAL):
                self.widget('pollbtn_lbl').set_text(btn.get_property('text'))
                break
        # Accounts
        for row in self.widget('accounts_lbox'):
            row.destroy()
        for account in A.list(self.config):
            AccountRow(self, account)
        # Plugins
        plugins_lbox = self.widget('plugins_lbox')
        for row in plugins_lbox:
            row.destroy()
        plugin_error = self.dbus_send(PLGSTATE_GET_METHOD, response=True)
        plugin_agregator = PluginAgregator(config=self.config)
        for plug_name in plugin_agregator.list():
            plugin = plugin_agregator[plug_name]
            PluginRow(self, plugin_error or list(), plug_name, plugin)
        self.ready = True

    @check_ready
    def on_window_resize(self, window, unused_value):
        window_size = list(map(str, window.get_size()))
        if 0 in window_size or self.window_size == window_size:
            return
        self.window_size = window_size

    def on_lbox_pages_row_activated(self, unused_tree, row):
        ''' Split between Accounts and plugins page '''
        page_request = row.get_name()
        main_stk = self.widget('main_stk')
        if page_request == main_stk.get_visible_child_name():
            return
        main_stk.set_visible_child_name(page_request)
        self.widget('add_btn').set_sensitive(page_request == 'accounts')

    def on_config_change(self, serialized_config):
        if self.config_lock:
            return
        self.config = C(load=False)
        self.config.load_serialized(serialized_config)
        self.ui_update()

    @check_ready
    def on_loglevel_changed(self, unused_widget, state):
        self.config.set_core(C.DEBUG, int(state))
        set_log_level(self.config)
        self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())

    @check_ready
    def on_aboutshow(self, unused_widget):
        if PACKAGE_NAME.capitalize() not in self.widget('app_name').get_text():
            pbuf = GdkPixbuf.Pixbuf.new_from_file(
                get_data_file(f'{PACKAGE_NAME}.png'))
            pbuf = pbuf.new_subpixbuf(0, 0, 128, 128)
            self.widget('app_icon').set_from_pixbuf(pbuf)
            self.widget('app_name').set_text(PACKAGE_NAME.capitalize())
            self.widget('app_version').set_text(str(APP_VERSION))
            self.widget('app_desc').set_markup(
                f'<a href="{APP_URL}">{PACKAGE_NAME.capitalize()}</a> - '
                + _(APP_DESC))
        self.widget('about_dlg').show()
        self.widget('about_dlg').run()
        self.widget('about_dlg').hide()

    def on_refresh(self, unused_widget):
        self.dbus_send(REFRESH_METHOD)

    @check_ready
    def on_logshow(self, unused_widget):
        for stack_name in 'global_stk', 'header_stk', 'leftbtn_stk':
            self.widget(stack_name).set_visible_child_name('log')
        self.widget('main_btn').hide()
        log_file = os.path.join(CACHE_DIR, f'{PACKAGE_NAME}.log')
        if not os.path.isfile(log_file):
            self.widget('logbuffer').set_text(_('No logfile found'))
            return
        log_content = ''
        try:
            with open(log_file, 'r') as log:
                log_content = log.read()
        except (PermissionError, OSError) as exc_error:
            log_content = exc_error.strerror
        self.widget('logbuffer').set_text(log_content)

    @check_ready
    def on_back(self, unused_widget):
        for stack_name in 'global_stk', 'header_stk', 'leftbtn_stk':
            self.widget(stack_name).set_visible_child_name('main')
        self.widget('main_btn').show()

    @check_ready
    def on_add(self, widget):
        if self.widget('main_stk').get_visible_child_name() != 'accounts':
            return
        backend = widget.get_name()
        is_local = backend in LocalAccount.BACKENDS
        account_class = LocalAccount if is_local else RemoteAccount
        self.new_account = account_class(self.config, {A.BACKEND: backend})
        if not is_local and self.config.getboolean(C.CORE, C.SHOW_AUTH_WARN):
            self.widget('auth_warn_dlg').show()
            return
        AccountDialog(self, self.new_account, create=True)

    @check_ready
    def on_poll_changed(self, widget):
        self.widget('pollbtn_lbl').set_text(widget.get_property('text'))
        self.config.set_core(C.POLL_INTERVAL, widget.get_name())
        self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())

    def on_auth_warn_dlg_closed(self, dialog, _unused_data=None):
        show_auth_warn = not self.widget('auth_warn_chk').get_active()
        dialog.hide()
        if show_auth_warn != bool(int(self.config.get_core(C.SHOW_AUTH_WARN))):
            self.config.set_core(C.SHOW_AUTH_WARN, show_auth_warn)
            self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())
        AccountDialog(self, self.new_account, create=True)

    @check_ready
    def on_window_deleted(self, unused_widget, unused_event=None):  # pylint: disable=R0201
        self.ready = False
        old_window_size = self.config.get_list(C.CORE, C.WINDOW_SIZE)
        if self.window_size != old_window_size:
            logging.debug('Saving new window size')
            self.config.set_core(C.WINDOW_SIZE,
                                 tuple(map(str, self.window_size)))
            self.dbus_send(CFG_SET_METHOD, arg=self.config.serialize())
        GLib.idle_add(Gtk.main_quit)

    def dbus_send(self, *signals, arg=None, response=False):
        pxy = self.dbus_proxy

        def _send(signal):
            try:
                logging.debug(f'Sending dbus signal "{signal}"')
                if arg is None:
                    resp = pxy.get_dbus_method(signal, DBUS_BUS_NAME)()
                else:
                    resp = pxy.get_dbus_method(signal, DBUS_BUS_NAME)(arg)
                return resp[0] if resp else None
            except Exception:
                logging.error(f'Dbus signal "{signal}"', exc_info=True)
                return None

        if response:
            return _send(signals[0])
        for signal in signals:
            GLib.idle_add(_send, signal)
