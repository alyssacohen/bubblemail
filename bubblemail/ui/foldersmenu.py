# Copyright 2019 - 2020 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import threading
# import time
import logging  # pylint: disable=W0611
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
# pylint: disable=C0413
from gi.repository import Gtk, GLib

from ..config import PACKAGE_NAME
from ..utils import get_data_file
from ..account import Account as A
from ..dbusservice import CFG_SET_METHOD
# from ..i18n import _
from .accountdialog import BACKEND_MAP

EXCLUDED = ('trash', 'sent', 'junk', 'spam', 'drafts', 'outbox', 'templates')

class FoldersMenu:
    def __init__(self, caller, menu_button, account):
        self.caller = caller
        self.account = account
        self.backend = BACKEND_MAP[self.account[A.BACKEND]](self.account)
        self.folder_list = None
        builder = Gtk.Builder()
        builder.set_translation_domain(PACKAGE_NAME)
        builder.add_from_file(get_data_file('folders_menu.ui'))
        builder.connect_signals({
            'on_folder_toggled': self.on_folder_toggled,
            'on_pop_folders_closed': self.on_pop_folders_closed})
        self.widget = builder.get_object
        menu_button.set_popover(builder.get_object('pop_folders'))
        menu_button.set_direction(Gtk.ArrowType.LEFT)

    def show(self):
        if self.folder_list:
            return
        self.widget('pop_folders').set_position(Gtk.PositionType.LEFT)
        if A.is_local(self.account):
            self.folder_list = self.backend.request_folders()
            self.append(self.folder_list, self.account[A.FOLDERS])
            return
        threading.Thread(target=self.get).start()
        self.widget('dialog_folders').hide()
        self.widget('folders_spinner').start()
        self.widget('folders_spinner').show()

    def get(self):
        # time.sleep(3)
        self.backend.open()
        self.folder_list = self.backend.request_folders()
        GLib.idle_add(self.append, self.folder_list, self.account[A.FOLDERS])

    def append(self, folder_list, enabled_folders):
        not_excluded = lambda f: not bool(list(
            filter(lambda e: e in f.lower(), EXCLUDED)))
        folder_list = list(sorted(filter(not_excluded, folder_list)))
        separator = self.backend.separator
        subfolder_map = list(filter(lambda f: separator in f, folder_list))
        base_folder_map = dict()
        item_count = 0
        spt_subfolder_map = map(lambda f: f.split(separator)[0], subfolder_map)
        for base_folder in set(spt_subfolder_map):
            base_folder_map[base_folder] = (self.widget('tst_folders').append(
                None, [base_folder, base_folder in enabled_folders]))
            item_count += 1
        for folder in filter(lambda f: f not in subfolder_map, folder_list):
            if folder in base_folder_map.keys():
                continue
            self.widget('tst_folders').append(
                None, [folder, folder in enabled_folders])
            item_count += 1
        for folder in subfolder_map:
            self.widget('tst_folders').append(
                base_folder_map[folder.split(separator)[0]],
                [separator.join(folder.split(separator)[1:]),
                 folder in enabled_folders])
            item_count += 1
        self.widget('treeview_folders').expand_all()
        self.set_height(item_count)
        self.widget('folders_spinner').stop()
        self.widget('folders_spinner').hide()
        self.widget('dialog_folders').show()

    def set_height(self, item_count):
        if not item_count:
            return
        height = item_count * 40
        caller_height = self.caller.window.get_size().height
        height = caller_height if height > caller_height else height
        self.widget('dialog_folders').set_min_content_height(height)

    def on_pop_folders_closed(self, unused_widget):
        self.account.save(create=False, commit=False)
        self.caller.dbus_send(CFG_SET_METHOD,
                              arg=self.account.config.serialize())

    def on_folder_toggled(self, cell, path):
        tst_folders = self.widget('tst_folders')
        folder = tst_folders.get_value(tst_folders.get_iter(path), 0)
        if len(path) > 1:
            root = tst_folders.get_value(tst_folders.get_iter(path[0]), 0)
            folder = f'{root}.{folder}'
        isactive = not cell.get_active()
        tst_folders.set_value(tst_folders.get_iter(path), 1, isactive)
        if isactive and folder not in self.account[A.FOLDERS]:
            self.account[A.FOLDERS].append(folder)
        if not isactive and folder in self.account[A.FOLDERS]:
            self.account[A.FOLDERS].remove(folder)
        # self.account.save(create=False, commit=False)
        # self.caller.dbus_send(CFG_SET_METHOD,
        #                       arg=self.account.config.serialize())
