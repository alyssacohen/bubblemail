# Copyright 2019 - 2020 razer <razerraz@free.fr>
# Copyright 2012 Patrick Ulbrich <zulu99@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
import os
import logging
import hashlib
from uuid import uuid4 as generate_uuid

import gi
gi.require_version('Secret', '1')
# pylint: disable=C0413
from gi.repository import Secret
try:
    gi.require_version('Goa', '1.0')
    from gi.repository import Goa
except (ValueError, ModuleNotFoundError):
    logging.warning('No gnome online accounts support found')
    Goa = None  # pylint: disable=C0103

from .config import PACKAGE_NAME, validate_folders
from .i18n import _


class SecretStore:
    def __init__(self):
        self.schema = Secret.Schema.new(
            f'org.framagit.{PACKAGE_NAME}', Secret.SchemaFlags.NONE,
            {"uuid": Secret.SchemaAttributeType.STRING})

    def save(self, account_name, uuid, password):
        if not uuid or not password:
            logging.error('Missing uuid or password')
            return
        Secret.password_store_sync(
            self.schema, {'uuid': uuid}, Secret.COLLECTION_DEFAULT,
            f'{PACKAGE_NAME.capitalize()} secret source for {account_name}',
            password, None)

    def password(self, uuid):
        return Secret.password_lookup_sync(self.schema, {'uuid': uuid}, None)

    def clear(self, uuid):
        return Secret.password_clear_sync(self.schema, {'uuid': uuid}, None)


class Account(dict):
    SECTION_NAME = 'Account '
    UUID = 'uuid'
    NAME = 'name'
    TYPE = 'type'
    ENABLED = 'enabled'
    BACKEND = 'backend'
    FOLDERS = 'folders'
    INTERNAL = 'internal'
    GOA = 'goa'
    IMAP = 'imap'
    POP3 = 'pop3'
    MAILDIR = 'maildir'
    MBOX = 'mbox'
    ERROR = 'error'
    STATUS = 'status'

    KEYS = (NAME, TYPE, ERROR, STATUS)
    OPT_KEYS = (ENABLED, FOLDERS)
    UNSAVED_KEYS = (FOLDERS,)

    def __init__(self, config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.config = config
        for accountkey in self.KEYS + self.OPT_KEYS:
            if accountkey not in self or self[accountkey] == 'None':
                self[accountkey] = ''
        if self.UUID not in self or not self[self.UUID]:
            self[self.UUID] = str(generate_uuid())[:8]
        if not self[self.TYPE]:
            self[self.TYPE] = self.INTERNAL
        if not self[self.ENABLED]:
            self[self.ENABLED] = '1'
        self[self.FOLDERS] = validate_folders(self[self.FOLDERS])
        self.section = self.SECTION_NAME + self[self.UUID]

    @classmethod
    def _account_class(cls, account):
        account_class = RemoteAccount
        if account[cls.TYPE] == cls.GOA:
            if not Goa:
                logging.warning('No gnome online accounts support found')
                return None
            return GnomeOnlineAccount
        if cls.is_local(account):
            account_class = LocalAccount
        return account_class

    @classmethod
    def get(cls, config, uuid):
        section = cls.SECTION_NAME + uuid
        if not section in config:
            logging.debug('Account %s not found in config', uuid)
            return None
        account = dict(config.items(section))
        account_class = cls._account_class(account)
        if account_class:
            return account_class(config, account)
        return None

    @classmethod
    def list(cls, config):
        A = Account
        sections = filter(lambda s: A.SECTION_NAME in s, config.sections())
        accounts = map(lambda s: dict(config.items(s)), sections)
        for account in sorted(accounts, key=lambda a: a[A.NAME].lower()):
            account_class = cls._account_class(account)
            if not account_class:
                continue
            yield account_class(config, account)

    @classmethod
    def count(cls, config):
        return len(list(cls.list(config)))

    @classmethod
    def is_local(cls, account):
        return account[cls.BACKEND] in (cls.MAILDIR, cls.MBOX)

    @property
    def is_locked(self):
        return False

    @property
    def is_valid(self):
        if list(filter(lambda k: k not in self, self.KEYS + self.OPT_KEYS)):
            return False
        filled_keys = filter(lambda k: k != self.ERROR, self.KEYS)
        filled_keys = filter(lambda k: k != self.STATUS, filled_keys)
        if list(filter(lambda k: not self[k], filled_keys)):
            return False
        return True

    def delete(self, safe_call=True, commit=True, keep_credentials=True):  # pylint: disable=unused-argument
        uuid = self[self.UUID]
        section = self.SECTION_NAME + uuid
        if safe_call and not self.config.has_section(section):
            logging.error(f'Unknown Account with id {uuid}')
            return
        self.config.remove_section(section)
        if commit:
            self.config.save()
        if self.config.dbus_service:
            self.config.dbus_service.Refresh()

    def set_status(self, status, error_message=None):
        """ Update account status and user's error message """
        self[self.STATUS] = status
        self[self.ERROR] = error_message or ''
        self.save(create=False)

    def set_folders(self):
        if not self.config.has_section(self.section):
            return
        if self[self.FOLDERS]:
            self.config.set(self.section, self.FOLDERS, self[self.FOLDERS])
        else:
            self.config.set(self.section, self.FOLDERS, '')

    def save(self, commit=True, create=True):
        if self.config.has_section(self.section):
            self.delete(safe_call=False, commit=False)
        elif not create:
            return
        self.config.add_section(self.section)
        for saved_key in filter(lambda key: key not in self.UNSAVED_KEYS,
                                self.keys()):
            self.config.set(self.section, saved_key, self[saved_key])
        if commit:
            self.config.save()


class LocalAccount(Account):
    PATH = 'path'
    BACKENDS = (Account.MAILDIR, Account.MBOX)

    KEYS = Account.KEYS + (Account.BACKEND, PATH)
    OPT_KEYS = Account.OPT_KEYS
    UNSAVED_KEYS = Account.UNSAVED_KEYS

    def __init__(self, config, *args, **kwargs):
        super().__init__(config, *args, **kwargs)
        self[self.BACKEND] = self[self.BACKEND] or self.MAILDIR

    @property
    def is_valid(self):
        if not super().is_valid:
            return False
        if not os.path.exists(self[self.PATH]):
            return False
        if not os.access(self[self.PATH], os.R_OK):
            return False
        if os.path.isdir(self[self.PATH]) and self[self.BACKEND] == self.MBOX:
            return False
        if os.path.isfile(self[self.PATH]):
            if self[self.BACKEND] == self.MAILDIR:
                return False
        return True

    def set_default_name(self):
        if self[self.NAME] or not self[self.PATH]:
            return
        self[self.NAME] = os.path.basename(self[self.PATH])
        self[self.NAME] = self[self.NAME].replace('.', '')
        self[self.NAME] = self[self.NAME].capitalize()

    def save(self, commit=True, create=True):
        super().save(commit=False, create=create)
        if self[self.BACKEND] == self.MAILDIR:
            self.set_folders()
        if commit:
            self.config.save()


class RemoteAccount(Account):
    OAUTH2 = 'oauth2'
    USER = 'user'
    PASS = 'password'
    SERVER = 'server'
    PORT = 'port'
    IMAP = 'imap'
    POP3 = 'pop3'
    BACKENDS = (IMAP, POP3)

    KEYS = Account.KEYS + (SERVER, USER, PASS, Account.BACKEND)
    OPT_KEYS = Account.OPT_KEYS + (PORT,)
    UNSAVED_KEYS = Account.UNSAVED_KEYS + (PASS,)

    def __init__(self, config, *args, **kwargs):
        super().__init__(config, *args, **kwargs)
        if not self.BACKEND in self.keys() or not self[self.BACKEND]:
            self[self.BACKEND] = self.IMAP
        self.secret_store = SecretStore()
        if self[self.TYPE] == self.INTERNAL and not self[self.PASS]:
            self[self.PASS] = self.secret_store.password(self[self.UUID])

    @property
    def is_locked(self):
        return not self[self.PASS]

    @property
    def is_valid(self):
        if not super().is_valid:
            return False
        if self[self.BACKEND] not in self.BACKENDS:
            return False
        return True

    def delete(self, safe_call=True, commit=True, keep_credentials=True):
        super().delete(safe_call, commit, keep_credentials)
        if self[self.TYPE] == self.INTERNAL and not keep_credentials:
            logging.debug(f'Deleting credentials for {self[self.NAME]}')
            self.secret_store.clear(self[self.UUID])

    def save(self, commit=True, create=True):
        super().save(commit=False, create=create)
        if self[self.TYPE] == self.INTERNAL and create:
            logging.debug(f'Saving credentials for {self[self.NAME]}')
            self.secret_store.save(self[self.NAME], self[self.UUID],
                                   self[self.PASS])
        if self[self.BACKEND] == self.IMAP:
            self.set_folders()
        if commit:
            self.config.save()


class GnomeOnlineAccount(RemoteAccount):
    OAUTH2 = 'oauth2'

    KEYS = Account.KEYS
    OPT_KEYS = RemoteAccount.OPT_KEYS + (Account.ENABLED, Account.FOLDERS)
    UNSAVED_KEYS = RemoteAccount.UNSAVED_KEYS + (
        RemoteAccount.USER, RemoteAccount.SERVER, RemoteAccount.PORT, OAUTH2)

    def __init__(self, config, *args, **kwargs):
        super().__init__(config, *args, **kwargs)
        self[self.USER] = 'Unknown user'
        self[self.SERVER] = 'Unknown server'
        goa_account = list(filter(
            lambda a: self.get_uuid(a.get_mail().props) == self[self.UUID],
            self.goa_accounts()))
        if goa_account:
            self.goa_account = goa_account[0]
            goa_mail = self.goa_account.get_mail().props
            self[self.UUID] = self.get_uuid(goa_mail)
            self[self.USER] = goa_mail.imap_user_name
            self[self.SERVER] = goa_mail.imap_host
            self.get_credentials()

    def get_credentials(self):
        pwd_based = self.goa_account.get_password_based()
        if pwd_based:
            self[self.PASS] = pwd_based.call_get_password_sync('imap-password',
                                                               None)
            return
        try:
            oauth2_based = self.goa_account.get_oauth2_based()
            auth_token = oauth2_based.call_get_access_token_sync(None)
            user_name = self.goa_account.get_mail().props.imap_user_name
        except Exception:
            logging.error('Getting GOA credentials', exc_info=True)
            self[self.ERROR] = _('Credential error')
            self.save()
        else:
            self[self.OAUTH2] = (
                f'user={user_name}\1auth=Bearer {auth_token[0]}\1\1')
            if self[self.ERROR]:
                self[self.ERROR] = ''
                self.save()

    @classmethod
    def is_local(cls, account):
        return False

    @property
    def is_locked(self):
        return self.OAUTH2 not in self or not self[self.OAUTH2]

    @classmethod
    def sync(cls, config, commit=True):
        # Clean up accounts deleted in GOA
        for account in cls.list(config):
            found = False
            for goa_account in cls.goa_accounts():
                goa_mail = goa_account.get_mail().props
                if account[cls.UUID] == cls.get_uuid(goa_mail):
                    found = True
                    break
            if not found:
                account.delete(commit=False, keep_credentials=False)
        # Add new accounts for new GOA accounts
        for goa_account in cls.goa_accounts():
            goa_mail = goa_account.get_mail().props
            uuid = cls.get_uuid(goa_mail)
            if cls.get(config, uuid):
                logging.debug('Goa account for %s already registered',
                              goa_mail.email_address)
                continue
            logging.info('Creating new Gnome account for %s(%s)',
                         goa_mail.imap_user_name, goa_mail.imap_host)
            account_dict = {
                cls.BACKEND:cls.IMAP, cls.UUID:uuid, cls.TYPE:cls.GOA,
                cls.NAME: _('Gnome account {0}').format(goa_mail.email_address),
                cls.SERVER:goa_mail.imap_host, cls.USER:goa_mail.imap_user_name}
            account = cls(config, account_dict)
            account.section = cls.SECTION_NAME + uuid
            account.save(commit=False)
        if commit:
            config.save()

    @staticmethod
    def is_available():
        return bool(Goa)

    @classmethod
    def cleanup(cls, config):
        logging.debug('Cleanup GOA accounts')
        # Remove imported Goa accounts
        for account in cls.list(config):
            logging.info('Deleting %s', account[cls.NAME])
            account.delete(commit=False)

    @classmethod
    def list(cls, config):
        return filter(lambda a: a[a.TYPE] == a.GOA, super().list(config))

    @staticmethod
    def get_uuid(mail_account):
        digest = hashlib.md5(mail_account.email_address.encode('utf-8'))
        return digest.hexdigest()[:8]

    @staticmethod
    def goa_accounts():
        client = Goa.Client.new_sync(None)
        accounts = client.get_accounts()
        for account in accounts:
            mail = account.get_mail() # get the mail interface
            if (not mail or account.get_account().props.mail_disabled
                    or not mail.props.imap_supported):
                continue
            yield account
