# Copyright 2016 Timo Kankare <timo.kankare@iki.fi>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

"""Backends to implement mail box specific functionality, like IMAP and POP3."""

from abc import ABCMeta, abstractmethod
import re
import email
import logging
import imaplib
import poplib
from ssl import SSLError
import socket

from .account import RemoteAccount as RA
from .utils import utf7_decode, utf7_encode
from .i18n import _

IMAP = 'imap'
POP3 = 'pop3'
BACKEND_MAP = {IMAP: imaplib.IMAP4, POP3: poplib.POP3}
SSL_BACKEND_MAP = {IMAP: imaplib.IMAP4_SSL, POP3: poplib.POP3_SSL}


class RemoteMail:
    """Interface for mailbox backends.
    Mailbox backend implements access to the specific type of mailbox.
    """

    __metaclass__ = ABCMeta

    UNKNOWN = 0
    FAILED = 1
    REFUSED = 2
    BAD_LOGIN = 3
    UNSECURE = 4
    CONNECT_ERROR = 5
    TIMEOUT = 6
    SUCCESS = 7
    OFFLINE = -3
    NOCRED_ERR = _('No credentials found !')
    AUTH_ERR = _('Authentification failed')
    INSECURE_ERR = _('Insecure connection')
    TIMEOUT_ERR = _('Connection timeout')
    REFUSED_ERR = _('Connection refused')
    UNKNOWN_ERR = _('Unknown error')

    def __init__(self, account):
        self.account = account
        self.backend = BACKEND_MAP[self.account[RA.BACKEND]]
        self.ssl_backend = SSL_BACKEND_MAP[self.account[RA.BACKEND]]
        self.connection = None
        # self.status = self.UNKNOWN
        socket.setdefaulttimeout(10)

    def open(self):  # pylint: disable=R0912, R0915
        """ Opens the mailbox. """

        if self.account.is_locked:
            logging.warning(f'Account {self.account[RA.NAME]}: avoiding '
                            + f'connect attempt without credentials !!!')
            self.account.set_status(self.FAILED, self.NOCRED_ERR)
            return None
        # account_error = ''
        account_name = self.account[RA.NAME]
        server = self.account[RA.SERVER]
        port = self.account[RA.PORT]
        if self.connection:
            logging.warning(f'{account_name}({self.account[RA.TYPE]}): '
                            + f'connection on {server} is already open')
            return self.connection
        unsecure = False

        def _open(backend, server, port):
            if port:
                self.connection = backend(server, port)
            else:
                self.connection = backend(server)
        try:
            _open(self.ssl_backend, server, port)
        except SSLError:
            try:
                _open(self.backend, server, port)
            except ConnectionRefusedError:
                self.account.set_status(self.BAD_LOGIN, self.REFUSED_ERR)
            except socket.gaierror:
                self.account.set_status(self.REFUSED, self.REFUSED_ERR)
            else:
                unsecure = True
                self.account.set_status(self.UNSECURE, self.INSECURE_ERR)
        except ConnectionRefusedError:
            self.account.set_status(self.BAD_LOGIN, self.REFUSED_ERR)
        except socket.gaierror as gaierror:
            account_error = None
            if gaierror.errno != self.OFFLINE:
                account_error = gaierror.strerror
            self.account.set_status(gaierror.errno, account_error)
        except OSError as oserror:
            self.account.set_status(oserror.errno, oserror.strerror)
        except Exception:
            logging.error(f'Unhandled exception', exc_info=True)
            self.account.set_status(self.UNKNOWN_ERR, 'Unhandled exception')
        else:
            if not unsecure:
                self.account.set_status(self.SUCCESS, '')
        if self.account[RA.STATUS] != self.SUCCESS:
            logging.error(f'{self.account[RA.BACKEND]} server {server} from '
                          + f'account {account_name}: {self.account[RA.ERROR]}')
        return self.connection

    @abstractmethod
    def close(self):
        """Closes the mailbox."""
        if not self.connection:
            return None
        return True

    @abstractmethod
    def is_open(self):
        """Returns true if mailbox is open."""
        raise NotImplementedError

    @abstractmethod
    def list_messages(self):
        """Lists unseen messages from the mailbox for this account.
           Yields tuples (folder, message) for every message.
           Be aware that return value need to be iterable
        """
        self.connection = self.connection if self.connection else self.open()
        if not self.is_open():
            return None
        return True

    @abstractmethod
    def request_folders(self):
        """Returns list of folder names available in the mailbox.
        Raises an exceptions if mailbox does not support folders.
        """
        raise NotImplementedError


class RemoteImap(RemoteMail):
    """Implementation of IMAP mail boxes."""
    MISSING_FOLDER = -1
    def __init__(self, account):
        super().__init__(account)
        self.separator = '.'

    def safe_call(self, command, *args, **kwargs):
        set_status = True
        exc_info = True
        if 'set_status' in kwargs.keys():
            set_status = kwargs['set_status']
            kwargs.pop('set_status')
        if 'exc_info' in kwargs.keys():
            exc_info = kwargs['exc_info']
            kwargs.pop('exc_info')

        def set_account_status(status, error):
            if set_status:
                self.account.set_status(status, error)

        err_str = f'{self.account[RA.NAME]}({self.account[RA.SERVER]}):'
        try:
            return self.SUCCESS, command(*args, **kwargs)
        except socket.timeout:
            logging.error(f'{err_str} timeout')
            set_account_status(self.TIMEOUT, self.TIMEOUT_ERR)
            return self.TIMEOUT, None
        except imaplib.IMAP4.error:
            logging.error(f'{err_str} imap error', exc_info=exc_info)
            set_account_status(self.CONNECT_ERROR, self.UNKNOWN_ERR)
            return self.CONNECT_ERROR, None
        except Exception:
            logging.error(f'{err_str} unknown exception', exc_info=exc_info)
            set_account_status(self.CONNECT_ERROR, self.UNKNOWN_ERR)
            return self.UNKNOWN, None

    def open(self):
        if not super().open():
            return None
        if RA.OAUTH2 in self.account and self.account[RA.OAUTH2]:
            status, _unused = self.safe_call(self.connection.authenticate,
                                             'XOAUTH2',
                                             lambda x: self.account[RA.OAUTH2])
        else:
            status, _unused = self.safe_call(self.connection.login,
                                             self.account[RA.USER],
                                             self.account[RA.PASS])
        if status == self.CONNECT_ERROR:
            self.account.set_status(self.BAD_LOGIN, self.AUTH_ERR)
        if status != self.SUCCESS:
            return None
        self.connection.utf8_enabled = True
        self.account.set_status(self.SUCCESS, '')
        status, response = self.safe_call(self.connection.namespace,
                                          set_status=False, exc_info=False)
        if status == self.SUCCESS and response and response[0] == 'OK':
            try:
                self.separator = str(response[1]).split()[1][1]
            except IndexError:
                pass
        return self.connection

    def close(self):
        if not super().close():
            return
        try:
            self.connection.select()
            self.connection.close()
            self.connection.logout()
        except imaplib.IMAP4.error:
            pass
        self.connection = None

    def is_open(self):
        return self.connection and self.connection.state != 'LOGOUT'

    def select_folder(self, folder):
        status, result = self.safe_call(
            self.connection.select, f'"{utf7_encode(folder)}"', readonly=True)
        if status == self.TIMEOUT:
            return self.TIMEOUT
        if status != self.SUCCESS or not result:
            return self.UNKNOWN_ERR
        if result[0] != 'OK':
            return self.MISSING_FOLDER
        return self.SUCCESS

    def fetch(self, folder, num):
        result = list()
        status, (_unused, msg_data) = self.safe_call(self.connection.fetch,
                                                     num, '(BODY.PEEK[HEADER])')
        if status != self.SUCCESS:
            return result
        for part in msg_data:
            if (not isinstance(part, (tuple, list)) or len(part) < 2
                    or not isinstance(part[1], (bytes, bytearray))):
                continue
            try:
                msg = email.message_from_bytes(part[1])
            except Exception:
                logging.error(f'Decoding email from imap: {part[1]}',
                              exc_info=True)
            else:
                result.append((folder, msg))
        return result

    def list_messages(self):
        if not super().list_messages():
            return None
        mail_map = []
        folder_map = ['INBOX']
        missing_folders = list()
        if RA.FOLDERS in self.account and self.account[RA.FOLDERS]:
            folder_map = self.account[RA.FOLDERS]
        for folder in folder_map:
            folder_select = self.select_folder(folder)
            if folder_select == self.TIMEOUT:
                break
            if folder_select == self.MISSING_FOLDER:
                missing_folders.append(folder)
            if folder_select != self.SUCCESS:
                continue
            status, (result, data) = self.safe_call(self.connection.search,
                                                    None, 'UNSEEN')
            if status == self.TIMEOUT:
                break
            if status != self.SUCCESS:
                continue
            if result != 'OK' or data == b'':
                logging.debug(f'Folder:{folder}, status:{status}, data:{data}')
                continue # Bugfix LP-735071
            for num in data[0].split():
                mail_map = mail_map + self.fetch(folder, num)
        if missing_folders:
            logging.warning(f'Folder(s) {missing_folders} missing, removing')
            for folder in missing_folders:
                self.account[RA.FOLDERS].remove(folder)
            self.account.save(create=False)
        return mail_map

    def request_folders(self):
        self.connection = self.connection if self.connection else self.open()
        if not self.is_open():
            return []
        status, (_unused, data_map) = self.safe_call(self.connection.list)
        if status != self.SUCCESS or not data_map:
            return []
        self.close()
        folder_list = list()
        for data in data_map:
            match = re.match(r'.+\s+("."|"?NIL"?)\s+"?([^"]+)"?$',
                             data.decode('utf-8'))
            if not match:
                logging.warning(f'{self.account[RA.SERVER]}: bad folder format')
                continue
            folder = utf7_decode(match.group(2))
            folder_list.append(folder)
        return folder_list


class RemotePop3(RemoteMail):
    """Implementation of POP3 mail boxes."""

    def open(self):
        if not super().open():
            return None
        err_str = f'{self.account[RA.NAME]}({self.account[RA.SERVER]}):'
        try:
            self.connection.getwelcome()
            self.connection.user(self.account[RA.USER])
            self.connection.pass_(self.account[RA.PASS])
        except poplib.error_proto:
            logging.error(f'{err_str} bad credentials')
            self.account.set_status(self.BAD_LOGIN, self.AUTH_ERR)
        except Exception:
            logging.error(f'{err_str} unhandled error', exc_info=True)
            self.account.set_status(self.CONNECT_ERROR, self.UNKNOWN_ERR)
        else:
            if self.account[RA.STATUS] != self.UNSECURE:
                self.account.set_status(self.SUCCESS, '')
            return self.connection
        return None

    def close(self):
        if not super().close():
            return
        try:
            self.connection.quit()
        except Exception:
            logging.error('Closing pop connection', exc_info=True)
        self.connection = None

    def is_open(self):
        return self.connection and 'sock' in self.connection.__dict__

    def list_messages(self):
        if not super().list_messages():
            return None
        folder = ''
        mail_map = list()
        # number of mails on the server
        mail_total = len(self.connection.list()[1])
        for i in range(1, mail_total + 1): # for each mail
            try:
                # header plus first 0 lines from body
                message = self.connection.top(i, 0)[1]
            except socket.timeout:
                logging.warning('Timeout when retrieving mails for pop3 '
                                + f'account {self.account[RA.NAME]}')
                break
            except Exception:  # pylint: disable=W0703
                logging.error('Error getting POP message', exc_info=True)
                continue
            try:
                msg = email.message_from_bytes(b'\n'.join(message))
            except Exception:
                logging.error(f'Decoding email from imap: {message}',
                              exc_info=True)
            else:
                mail_map.append((folder, msg))
        return mail_map

    def request_folders(self):
        raise NotImplementedError('POP3 does not support folders')
