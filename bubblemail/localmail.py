# Copyright 2019 - 2020 razer <razerraz@free.fr>
# Copyright 2016 Timo Kankare <timo.kankare@iki.fi>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import os
from glob import glob
import logging
import getpass
import mailbox

from .i18n import _
from .account import Account as A, LocalAccount as LA

class LocalMail:
    """Implementation of local mailboxes, like mbox and maildir."""
    ERROR = 0
    SUCCESS = 7
    INVALID_ERR = _('No valid local mail folder/file found')

    @classmethod
    def search_paths(cls, config):

        def add_accounts(backend, path_map):
            for count, path in enumerate(path_map):
                account = LA(config, {A.BACKEND:backend, LA.PATH:path})
                account.set_default_name()
                if account[A.NAME] in map(lambda a: a[A.NAME], A.list(config)):
                    account[A.NAME] = f'{account[A.NAME]} {count}'
                account.save()

        home = os.getenv('HOME')
        add_accounts(A.MAILDIR, glob(os.path.join(home, 'Maildir*')))
        add_accounts(A.MBOX, glob(os.path.join(home, '.mbox*'))
                     + glob(os.path.join('/var/spool/mail', getpass.getuser())))

    def __init__(self, account):
        """Initialize mbox mailbox backend with a name and path."""
        self.account = account
        self.separator = '.'

    @property
    def available(self):
        if not isinstance(self.account, LA):
            logging.error('LocalMail class loaded with bad account argument!')
            return False
        path = self.account[LA.PATH]
        log_message = f'Local account {self.account[A.NAME]}'
        if not path or not os.access(path, os.R_OK):
            logging.warning(f'{log_message}: invalid local mail folder/file')
            self.account.set_status(self.ERROR, error_message=self.INVALID_ERR)
            return False
        backend = self.account[A.BACKEND]
        if backend != A.MAILDIR and not os.path.isfile(path):
            logging.warning(f'{log_message}: path must be a file !')
            self.account.set_status(self.ERROR, error_message=self.INVALID_ERR)
            return False
        if backend != A.MBOX and not os.path.isdir(path):
            logging.warning(f'{log_message}: path must be a folder !')
            self.account.set_status(self.ERROR, error_message=self.INVALID_ERR)
            return False
        self.account.set_status(self.SUCCESS)
        return True

    @property
    def is_browsable(self):
        return self.account[A.BACKEND] == A.MAILDIR

    def list_messages(self):
        """List unread messages from the mailbox.
        Yields pairs (folder, message) where folder is always ''.
        """
        if not self.available:
            return None
        mail_map = list()
        if self.account[A.BACKEND] == A.MBOX:
            logging.debug('Checking mbox %s', self.account[LA.PATH])
            mbox = mailbox.mbox(self.account[LA.PATH], create=False)
            for mail in mbox:
                if 'R' not in mail.get_flags():
                    mail_map.append(('', mail))
            mbox.close()
            return mail_map
        maildir = mailbox.Maildir(self.account[LA.PATH], factory=None,
                                  create=False)
        folder_map = ['INBOX']
        if A.FOLDERS in self.account and self.account[A.FOLDERS]:
            folder_map = self.account[A.FOLDERS]
        for folder in folder_map:
            try:
                for mail in self._get_folder(maildir, folder):
                    if 'S' not in mail.get_flags():
                        mail_map.append((folder, mail))
            except (FileNotFoundError, PermissionError):
                logging.exception(
                    f'Getting local folder {self.account[LA.PATH]}/{folder}',
                    exc_info=True)
                continue
        maildir.close()
        return mail_map

    def request_folders(self):
        """Lists folders from maildir."""
        if not self.available or not self.is_browsable:
            return None
        maildir = mailbox.Maildir(self.account[LA.PATH], factory=None,
                                  create=False)
        folders = maildir.list_folders()
        maildir.close()
        return folders

    def _get_folder(self, maildir, folder):
        if not folder or folder.lower() == 'inbox':
            return maildir
        try:
            return maildir.get_folder(folder)
        except mailbox.NoSuchMailboxError:
            logging.warning(f'Local folder {folder} not found, removing it')
            self.account[A.FOLDERS].remove(folder)
            self.account.save()
            return list()
