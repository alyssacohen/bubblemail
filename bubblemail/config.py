# Copyleft 2019 razer <razerraz@free.fr>
# Copyright 2011 - 2015 Patrick Ulbrich <zulu99@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

import os
import logging
import builtins
import copy
from configparser import RawConfigParser, Error as ConfigParserError
import xdg.BaseDirectory as bd


PACKAGE_NAME = 'bubblemail'
APP_VERSION = '0.79'
MIN_COMPAT = 0.29
APP_URL = 'https://framagit.org/razer/bubblemail'
RELEASE_URL = 'https://framagit.org/api/v4/projects/49237/releases'
DOWNLOAD_URL = 'http://bubblemail.free.fr'
APP_DESC = 'An extensible mail notification service'
DEVMODE = hasattr(builtins, 'BB_DEVMODE') and builtins.BB_DEVMODE  # pylint: disable=E1101
if DEVMODE and hasattr(builtins, 'BB_DEVHOMEPATH'):
    CONFIG_DIR = os.path.join(builtins.BB_DEVHOMEPATH, '.config')  # pylint: disable=E1101
    CONFIG_DIR = os.path.join(CONFIG_DIR, PACKAGE_NAME)
    CACHE_DIR = os.path.join(builtins.BB_DEVHOMEPATH, '.cache')  # pylint: disable=E1101
    CACHE_DIR = os.path.join(CACHE_DIR, PACKAGE_NAME)
else:
    CONFIG_DIR = os.path.join(bd.xdg_config_home, PACKAGE_NAME)
    CACHE_DIR = os.path.join(bd.xdg_cache_home, PACKAGE_NAME)
DBUS_BUS_NAME = f'{PACKAGE_NAME}.{PACKAGE_NAME.capitalize()}Service'
DBUS_OBJ_PATH = f'/{DBUS_BUS_NAME.replace(".", "/")}'
SEPARATOR = ', '
NEWLINE = '\n'
FAKESPACE = '_SPACE_'

class Config(RawConfigParser):
    CORE = 'core'
    VERSION = 'version'
    DEBUG = 'debug'
    POLL_INTERVAL = 'poll_interval'
    ENABLED_PLUGINS = 'enabled_plugins'
    PLUGINS_ORDER = 'plugins_order'
    SHOW_AUTH_WARN = 'show_auth_warn'
    UNAVAILABLE = 'unavailable'
    # UPDATE_AVAILABLE = 'update_available'
    WINDOW_SIZE = 'window_size'
    CORE_DEFAULTS = {
        VERSION: APP_VERSION, DEBUG:'0', POLL_INTERVAL:'10',
        SHOW_AUTH_WARN: '1', # UPDATE_AVAILABLE: '0',
        ENABLED_PLUGINS: 'libnotifyplugin', WINDOW_SIZE: '0, 0',
        PLUGINS_ORDER: ('spamfilterplugin, userscriptplugin, libnotifyplugin, '
                        + 'soundplugin')}

    def __init__(self, *args, load=True, dbus_service=None, on_saved_cb=None,
                 **kwargs):
        super().__init__(*args, **kwargs)
        self.on_saved_cb = on_saved_cb or (lambda: None)
        self._tmp_file = os.path.join(CACHE_DIR, f'{PACKAGE_NAME}.cfg')
        self._file = os.path.join(CONFIG_DIR, f'{PACKAGE_NAME}.cfg')
        self.dbus_service = dbus_service
        self._saved_state = None
        self.is_new = False
        if load:
            self.load()

    def get_list(self, section, option):
        return str_to_list(self.get(section, option))

    def get_core(self, option, array=False):
        if array:
            return self.get_list(self.CORE, option)
        return self.get(self.CORE, option)

    def set(self, section, option, value=None):
        if value is None:
            value = ''
        if isinstance(value, (list, tuple)):
            value = list_to_str(value)
        elif isinstance(value, bool):
            value = str(int(value))
        elif isinstance(value, (int, float)):
            value = str(value)
        assert isinstance(value, str), f'Error setting value type {type(value)}'
        super().set(section, option, value)

    def set_core(self, option, value):
        self.set(self.CORE, option, value)

    def _generate_new(self, error_message=None, warn_message=None):
        if error_message:
            logging.error(error_message)
        if warn_message:
            logging.warning(warn_message)
        self[self.CORE] = self.CORE_DEFAULTS
        self.is_new = True
        self.save()

    def _check_version(self):
        previous_version = self.get_core(self.VERSION)
        if previous_version == APP_VERSION:
            return False
        previous_version = previous_version.replace('beta', '')
        previous_version = previous_version.replace('alpha', '')
        try:
            previous_version = float(previous_version)
        except ValueError:
            self._generate_new(
                error_message=f'Incorrect version: {previous_version}')
            return None
        if previous_version < MIN_COMPAT:
            self._generate_new(error_message='Incompatible config file')
            return None
        self.set_core(self.VERSION, APP_VERSION)
        return True

    def load_serialized(self, serialized_config):
        config = dict()
        for section in serialized_config:
            config[section['section_name']] = section
            del config[section['section_name']]['section_name']
        self.clear()
        self.read_dict(config)

    def load(self):
        if not os.path.exists(CACHE_DIR):
            os.makedirs(CACHE_DIR)
        if not os.path.exists(CONFIG_DIR):
            os.makedirs(CONFIG_DIR)
            self._generate_new()
            return
        if not os.path.exists(self._file):
            self._generate_new(warn_message='No config file found')
            return
        try:
            self.read(self._file)
        except (ConfigParserError, UnicodeDecodeError):
            self._generate_new(error_message='Config file is corrupted')
            return
        except PermissionError:
            logging.error('', exc_info=True)
            return
        if self.CORE not in self.sections():
            self._generate_new(error_message='No core session found in config')
            return
        save_needed = False
        # Clean up keys that are not used anymore
        for config_key in self[self.CORE].keys():
            if config_key not in self.CORE_DEFAULTS.keys():
                self.remove_option(self.CORE, config_key)
                save_needed = True
        # Add missing keys
        for config_key, default_value in self.CORE_DEFAULTS.items():
            if config_key not in self[self.CORE]:
                logging.info('Adding %s->%s', config_key, default_value)
                self.set_core(config_key, default_value)
                save_needed = True
        if self._check_version() or save_needed:
            self.save()
        logging.debug('Config loaded')

    def enable_plugin(self, plug_name):
        enabled_plugins = self.get_core(self.ENABLED_PLUGINS, array=True)
        if plug_name in enabled_plugins:
            logging.warning(f'Plugin {plug_name} already in ENABLED_PLUGINS!')
            return
        enabled_plugins.append(plug_name)
        self.set_core(self.ENABLED_PLUGINS, enabled_plugins)
        self.save()

    def disable_plugin(self, plug_name):
        enabled_plugins = self.get_core(self.ENABLED_PLUGINS, array=True)
        if not enabled_plugins or plug_name not in enabled_plugins:
            logging.warning(f'Plugin {plug_name} not in ENABLED_PLUGINS!')
            return
        enabled_plugins.remove(plug_name)
        self.set_core(self.ENABLED_PLUGINS, enabled_plugins)
        self.save()

    def serialize(self):
        def serialized_section(item):
            section, content = item
            section_content = dict(content)
            section_content['section_name'] = section
            return section_content
        return list(map(serialized_section, self.items()))

    def save(self):
        if self._saved_state and self._saved_state == self._sections:
            return
        logging.debug('Saving config')
        self.on_saved_cb()
        with open(self._file, 'w') as configfile:
            self.write(configfile)
        if self.dbus_service:
            logging.debug('Emitting "ConfigUpdate" dbus signal')
            self.dbus_service.ConfigUpdate(self.serialize())
        self._saved_state = copy.deepcopy(self._sections)

    @property
    def log_level(self):
        debug = int(self.get(self.CORE, self.DEBUG))
        return logging.DEBUG if debug else logging.INFO

def str_to_list(config_value):
    list_value = filter(len, config_value.split(SEPARATOR))
    list_value = filter(lambda c: not c.isspace(), list_value)
    return list(map(lambda c: c.replace(FAKESPACE, ' '), list_value))

def list_to_str(list_value):
    if isinstance(list_value, str):
        return list_value
    list_value = map(lambda c: c.replace(' ', FAKESPACE), list_value)
    return SEPARATOR.join(filter(len, list_value))

def validate_folders(folder_map):
    if isinstance(folder_map, (dict, tuple, list)):
        return list(filter(len, folder_map))
    if not folder_map:
        return []
    return str_to_list(folder_map)
