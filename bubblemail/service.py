# Copyright 2014, 2015 Patrick Ulbrich <zulu99@gmx.net>
# Copyright 2016 Timo Kankare <timo.kankare@iki.fi>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import sys
import threading
import logging
import signal
import time
import dbus
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib

from .utils import (init_logging, shutdown_service, set_log_level)
                    # version_checker)
from .account import Account as A, GnomeOnlineAccount
from .mail import Mail, MailCache
from .localmail import LocalMail
from .dbusservice import DBusService
from .plugin import PluginAgregator
from .config import Config, PACKAGE_NAME #, APP_VERSION , RELEASE_URL

RUNNING = 'running'
STOPPED = 'stopped'
LAST_CHECK = 'last_check'
MAIN = 'main'
LOOP = 'loop'
STOP_LOOP = 'stop_poll'

NM_CONNECTED = 70
NM_PATH = '/org/freedesktop/NetworkManager'
NM_NAME = 'org.freedesktop.NetworkManager'
DBUS_PROPS_NAME = 'org.freedesktop.DBus.Properties'

DBusGMainLoop(set_as_default=True)


class Service:
    def __init__(self):
        self.dbus_service = None
        self.config = None
        self.cache = None
        self.plugins = None
        self.on_refresh = False
        self.refresh_thread = None
        self.thread_event = threading.Event()
        self.online = False
        self.running = False
        self.last_unseen_count = 0
        init_logging()

    @property
    def poll_interval(self):
        return int(self.config.get_core(self.config.POLL_INTERVAL))

    def _nm_change_handler(self, state):
        """ Handler triggered by NetworkManager on connection changes """
        if self.online == (state == NM_CONNECTED):
            return
        self.online = state == NM_CONNECTED
        logging.debug('Connect state is now %s', self.online)
        if self.online:
            def _refresh():
                # Give connection time to set up
                time.sleep(5)
                self.refresh()
            threading.Thread(target=_refresh).start()

    def run(self):
        """ Initializes the service and starts checking threads. """
        # Shutdown running instances if any
        shutdown_service()
        self.dbus_service = DBusService(self)
        self.config = Config(dbus_service=self.dbus_service,
                             on_saved_cb=self.on_config_saved)
        set_log_level(self.config)
        # Check for new version
        # self.config.set_core(Config.UPDATE_AVAILABLE,
        #                      version_checker(RELEASE_URL, APP_VERSION))
        self.cache = MailCache()
        bus = dbus.SystemBus()
        # Stop everything properly on kill
        for stop_signal in signal.SIGINT, signal.SIGTERM:
            GLib.unix_signal_add(GLib.PRIORITY_HIGH, stop_signal, self.stop)
        # Connect to networkmanager connect state signal
        if bus.name_has_owner(NM_NAME):
            proxy = bus.get_object(NM_NAME, NM_PATH)
            proxy.connect_to_signal('StateChanged', self._nm_change_handler)
        # Load plugins
        self.plugins = PluginAgregator(service=self)
        self.plugins.enable()
        self.running = True
        if self.config.is_new:
            LocalMail.search_paths(self.config)
        self.refresh_thread = threading.Thread(target=self.refresh_loop,
                                               daemon=True)
        self.refresh_thread.start()
        logging.debug(f'{PACKAGE_NAME.capitalize()} service ready')
        GLib.MainLoop().run()

    def stop(self, unused_signum=None, unused_frame=None):
        if not self.running:
            return
        logging.info('Stopping service')
        self.running = False
        if self.refresh_thread and self.refresh_thread.is_alive():
            self.thread_event.set()
            logging.debug('Waiting for threads to finish')
            self.refresh_thread.join()
        logging.debug('Stopping GLib loop')
        GLib.MainLoop().quit()
        logging.info('Bye...')
        sys.exit(0)


    def refresh(self, first_check=False, force=False):
        """ Check local and remote accounts for new mails
            If first_check, consider all unreaded mails as new ones
        """
        if self.on_refresh:
            logging.info('A collect is already running')
            return
        if not force and not self.poll_interval:
            logging.info('Auto refresh disabled in config')
            return
        self.on_refresh = True
        if GnomeOnlineAccount.is_available():
            GnomeOnlineAccount.sync(self.config)
        else:
            if first_check:
                logging.warning('No gnome online accounts support found')
            GnomeOnlineAccount.cleanup(self.config)
        # ON_COLLECT plugin hook triggering
        accounts = self.plugins.run_hook('on_collect',
                                         list(A.list(self.config)))
        if not accounts or not list(filter(lambda a: a[A.ENABLED], accounts)):
            logging.info('No enabled account found, bypass collect')
            self.on_refresh = False
            return
        # Collect mails
        new_mails, unseen_mails = Mail.collect(
            accounts, self.cache, first_check=first_check)
        if new_mails or self.last_unseen_count != len(unseen_mails):
            # ON_UPDATE plugin hook triggering
            new_mails, unseen_mails = self.plugins.run_hook(
                'on_update', [new_mails, unseen_mails])
            self.dbus_service.on_update(unseen_mails)
        self.last_unseen_count = len(unseen_mails)
        self.on_refresh = False

    def on_config_saved(self):
        if self.plugins:
            self.config = self.plugins.run_hook('on_config_saved', self.config)

    def refresh_loop(self):
        # First Mail collect
        self.refresh(first_check=True)
        while True:
            # Check for changes every 3 minutes if poll disabled
            self.thread_event.wait(timeout=60*self.poll_interval or 180)
            if self.thread_event.is_set():
                break
            if self.poll_interval:
                self.refresh()
