# Copyright 2019 - 2020 razer <razerraz@free.fr>
# Copyright 2011 - 2016 Patrick Ulbrich <zulu99@gmx.net>
# Copyright 2016, 2018 Timo Kankare <timo.kankare@iki.fi>
# Copyright 2011 Leighton Earl <leighton.earl@gmx.com>
# Copyright 2011 Ralf Hersel <ralf.hersel@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

import os
import logging
import hashlib
import email
from email.header import make_header, decode_header
from uuid import uuid4 as generate_uuid
from .config import CACHE_DIR
from .utils import get_avatars, get_data_file, pickle_read, pickle_write
from .account import Account as A
from .remotemail import RemoteImap, RemotePop3
from .localmail import LocalMail
from .i18n import _


class Mail(dict):
    """ Mail dictionary storage helper
            - All accounts collect method
            - From email object raw creation
            - Headers storage as dictionnary
    """
    DATETIME = 'datetime'
    SUBJECT = 'subject'
    NAME = 'name'
    ADDRESS = 'address'
    AVATAR = 'avatar'
    UUID = 'uuid'
    ACCOUNT = 'account'
    KEYS = (UUID, ACCOUNT, DATETIME, SUBJECT, NAME, ADDRESS)
    BACKEND_MAP = {A.IMAP: RemoteImap, A.POP3: RemotePop3, A.MAILDIR: LocalMail,
                   A.MBOX: LocalMail}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self:
            for mailkey in self.KEYS:
                self[mailkey] = None

    def __eq__(self, other):
        """ compare itself with an uuid string """
        if isinstance(other, str):
            return other == self[self.UUID]
        return super().__eq__(other)

    @property
    def sender(self):
        return self[self.NAME] or self[self.ADDRESS]

    @staticmethod
    def from_raw(account, folder, raw_email):
        """ Create new instance from raw  """
        # Get all keys lowercase
        raw_email = {k.lower(): v for k, v in raw_email.items()}
        for key in 'subject', 'from', 'date':
            raw_email[key] = '' if key not in raw_email else raw_email[key]
        mail = Mail()
        mail[Mail.ACCOUNT] = account
        mail[Mail.SUBJECT] = mail.convert(raw_email['subject'])
        mail[Mail.SUBJECT] = mail[Mail.SUBJECT] or _('No subject')
        try:
            mail_from = email.utils.parseaddr(mail.convert(raw_email['from']))
        except TypeError:
            logging.error(f'Decoding raw mail {raw_email}', exc_info=True)
            mail_from = ('', _('Unknown sender'))
        if len(mail_from) < 2:
            logging.error(f'Malformed "From" header: {mail_from}')
            mail_from = ('', _('Unknown sender'))
        mail_from_conversion = map(mail.convert, mail_from)
        mail[Mail.NAME] = next(mail_from_conversion)
        mail[Mail.ADDRESS] = next(mail_from_conversion)
        try:
            parsed_date = email.utils.parsedate_tz(raw_email['date'])
            # Datetime in unix format
            mail[Mail.DATETIME] = str(email.utils.mktime_tz(parsed_date))
        except TypeError:
            logging.error(f'Getting date from {raw_email}', exc_info=True)
            return None
        try:
            mail[Mail.UUID] = hashlib.md5(
                (account + folder + mail[Mail.NAME] + mail[Mail.ADDRESS]
                 + mail[Mail.SUBJECT] + mail[Mail.DATETIME])
                .encode('utf-8')).hexdigest()
        except IndexError:
            logging.error(f'Generating uuid for {mail}', exc_info=True)
            mail[Mail.UUID] = str(generate_uuid())[:8]
        return mail

    @classmethod
    def convert(cls, input_text):
        """ return utf-8 decoded string from charset specific input_text """
        if not input_text:
            return ''
        if isinstance(input_text, email.header.Header):
            input_text = input_text.encode('utf-8')
        elif not isinstance(input_text, str):
            return ''
        try:
            decoded = decode_header(input_text)
        except TypeError:
            logging.error('', exc_info=True)
            return ''

        def add_encoding(decode_result):
            if decode_result[1] and 'unknown' in decode_result[1]:
                return decode_result[0], 'utf-8'
            return decode_result

        decoded = list(map(add_encoding, decoded))
        try:
            return str(make_header(decoded))
        except UnicodeDecodeError:
            logging.error('', exc_info=True)
            return ''

    @staticmethod
    def sort(mail_map, reverse=False):
        return sorted(mail_map, key=lambda m: int(m[m.DATETIME]),
                      reverse=reverse)

    @staticmethod
    def collect(accounts, cache, first_check=False):  # pylint: disable=R0914
        """ Collect new messages from local and remote accounts """
        logging.info('Collecting mail...')
        mail_map = []
        # Avatar Update
        avatar_map = get_avatars()

        def append(account, folder, message):
            mail = Mail.from_raw(account, folder, message)
            if not mail:
                return
            if list(filter(lambda m: m[m.UUID] == mail[mail.UUID], mail_map)):
                logging.warning(f'Duplicate email detected: {mail}')
                return
            if avatar_map and mail[mail.ADDRESS].lower() in avatar_map:
                mail[mail.AVATAR] = avatar_map[mail[mail.ADDRESS].lower()]
            else:
                valid_chars = list(filter(str.isalpha, mail.sender.upper()))
                avatar_file = valid_chars[0] if valid_chars else 'A'
                default_avatar = get_data_file(f'avatars/{avatar_file}.png')
                mail[mail.AVATAR] = default_avatar or ''
            mail_map.append(mail)

        for account in accounts:
            logging.debug(f'Collecting from {account[A.NAME]}')
            if not int(account[A.ENABLED]):
                logging.debug('Account %s is disabled', account[A.NAME])
                continue
            mailbox = Mail.BACKEND_MAP[account[A.BACKEND]](account)
            account_mails = mailbox.list_messages()
            if hasattr(mailbox, 'status') and account_mails is None:
                logging.debug(f'Error on {account[A.NAME]}(remote): keeping '
                              + 'previous collect results')
                mail_map += list(
                    filter(lambda m, a=account: m[m.ACCOUNT] == a[A.UUID],
                           cache.mail_map))
                continue
            if account_mails:
                for folder, message in account_mails:
                    append(account[account.UUID], folder, message)
            if hasattr(mailbox, 'close'):
                mailbox.close()
        sorted_map = Mail.sort(mail_map)
        unseen_map = list(filter(
            lambda m: not(cache.is_dismissed(m[m.UUID])), sorted_map))
        new_map = unseen_map if first_check else list(filter(
            lambda m: not cache.contains(m[m.UUID]), sorted_map))
        cache.sync(mail_map)
        if accounts:
            logging.info(f'{len(accounts)} account(s) have '
                         + f'{len(unseen_map)} unseen mails and '
                         + f'{len(new_map)} new mails')
        return new_map, unseen_map


class MailCache(dict):
    def __init__(self):
        dict.__init__(self)
        self.filepath = os.path.join(CACHE_DIR, 'mails.cache')
        self.account_map = dict()
        self.mail_map = list()
        self.load()

    def load(self):
        self.clear()
        cache_content = pickle_read(self.filepath)
        if not cache_content:
            return
        for uuid, cache_data in cache_content.items():
            self[uuid] = cache_data

    def save(self):
        pickle_write(self, self.filepath)

    # def sync(self, accounts, mail_map):
    def sync(self, mail_map):
        self.mail_map = mail_map.copy()
        deleted_uuids = list(filter(lambda m: m not in self.mail_map, self))
        deleted_uuids = list(map(self.delete, deleted_uuids))
        new_mails = filter(lambda m: not self.contains(m[m.UUID]), mail_map)
        new_mails = list(map(self.add, new_mails))
        if new_mails or deleted_uuids:
            logging.debug('Saving cache')
            self.save()

    def contains(self, uuid):
        """ check if mail id is in the cache list """
        return uuid in self

    def add(self, item, dismissed=False):
        self[item[Mail.UUID]] = [dismissed, item[Mail.ACCOUNT]]

    def delete(self, item):
        if item not in self:
            logging.warning(f'Mail with uuid {item} not found')
            return
        del self[item]

    def dismiss(self, uuid, commit=True):
        if self.is_dismissed(uuid):
            logging.warning(f'Mail with id {uuid} already dismissed')
            return
        self[uuid][0] = True
        if commit:
            self.save()

    def dismiss_all(self):
        """ Set dismissed flag on every cache items"""
        for uuid in self:
            self.dismiss(uuid, commit=False)
        self.save()

    def is_dismissed(self, uuid):
        if not self.contains(uuid):
            return False
        return self[uuid][0]
