#!/usr/bin/env python3
# Copyright 2013 - 2016 Patrick Ulbrich <zulu99@gmx.net>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
from bubblemail.plugin import Plugin
from bubblemail.i18n import _

class SpamfilterPlugin(Plugin):
    MANIFEST = (
        _("Spam Filter"), _("Filters out unwanted mails."),
        "1.1.1", "Patrick Ulbrich <zulu99@gmx.net>, Razer <razerraz@free.fr")
    DEFAULT_CONFIG = {'filter_list' : 'newsletter, viagra'}

    @property
    def filter_list(self):
        return self.config['filter_list'].strip('\n').strip(' ').split(',')

    @property
    def txt_buffer(self):
        return self.builder.get_object('txt_buffer')

    def load_settings(self):
        self.txt_buffer.set_text(self.config['filter_list'])

    def save_settings(self):
        start, end = self.txt_buffer.get_bounds()
        self.config['filter_list'] = self.txt_buffer.get_text(start, end, False)

    def on_update(self, mails):
        new_mails, unseen_mails = mails
        return (list(filter(self.mail_filter, new_mails)),
                list(filter(self.mail_filter, unseen_mails)))

    def mail_filter(self, mail):
        is_spam = False
        for filter_exp in self.filter_list:
            # remove CR and white space
            filter_exp = filter_exp.strip()
            if not filter_exp:
                continue
            filter_exp = filter_exp.lower()
            if filter_exp in (mail[mail.NAME].lower(),
                              mail[mail.ADDRESS].lower(),
                              mail[mail.SUBJECT].lower()):
                # sender or subject contains filter string
                is_spam = True
                break
        return not is_spam
