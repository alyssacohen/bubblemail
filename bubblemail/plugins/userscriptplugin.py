#!/usr/bin/env python3
# Copyright 2013 - 2016 Patrick Ulbrich <zulu99@gmx.net>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

import os
from copy import deepcopy
from subprocess import Popen, PIPE
import logging

from bubblemail.plugin import Plugin
from bubblemail.i18n import _


class UserscriptPlugin(Plugin):
    MANIFEST = (
        _('User Script'), _('Runs an user defined script on mail arrival.'),
        '1.2.1', 'Patrick Ulbrich <zulu99@gmx.net>, Razer <razerraz@free.fr')
    DEFAULT_CONFIG = {'script_file': ''}

    @property
    def filechooser(self):
        return self.builder.get_object('filechooser')

    def load_settings(self):
        if self.config['script_file']:
            self.filechooser.set_filename(self.config['script_file'])

    def save_settings(self):
        self.config['script_file'] = self.filechooser.get_filename() or ''

    def on_update(self, mails):
        if not mails[0]:
            return mails
        script_file = self.config['script_file'].strip()
        if not script_file:
            return mails
        if not os.path.exists(script_file):
            logging.warning(f'Script file {script_file} not found')
            return mails
        new_mails = deepcopy(mails[0])
        script_args = [script_file, str(len(new_mails))]
        for mail in new_mails:
            script_args.append(mail[mail.ACCOUNT])
            script_args.append(f'{mail[mail.NAME]} <{mail[mail.ADDRESS]}>')
            script_args.append(mail[mail.SUBJECT])
        try:
            script_proc = Popen(script_args, stderr=PIPE)
        except Exception:
            logging.error(f'Running {script_args}', exc_info=True)
        else:
            error = script_proc.communicate()
            if error[1]:
                logging.error(error[1].decode('utf-8'))
        return mails
