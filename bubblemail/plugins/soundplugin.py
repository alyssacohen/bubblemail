# Copyright 2013 - 2016 Patrick Ulbrich <zulu99@gmx.net>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import os
import threading
import logging

import gi
gi.require_version('Gst', '1.0')
# pylint: disable=C0413
try:
    from gi.repository import Gst
except ModuleNotFoundError:
    Gst = None  # pylint: disable=C0103

from bubblemail.plugin import Plugin
from bubblemail.utils import get_data_file
from bubblemail.config import PACKAGE_NAME
from bubblemail.i18n import _


class SoundPlugin(Plugin):
    MANIFEST = (
        _("Sound Notifications"), _("Plays a sound when new mails arrive."),
        "1.1.1", "Patrick Ulbrich <zulu99@gmx.net>, Razer <razerraz@free.fr")
    DEFAULT_CONFIG = {'soundfile' : '', 'volume': '1.0'}

    def __init__(self):
        self.soundfile = ''
        self.volume = 1.0

    @property
    def available(self):
        return bool(Gst)

    def on_update(self, mails):
        if mails[0]:
            gstplay(self.get_valid(self.config['soundfile']),
                    float(self.config['volume']))
        return mails

    def load_settings(self):
        self.soundfile = self.config['soundfile']
        self.volume = float(self.config['volume'])
        custom_sound = bool(self.soundfile)
        self.builder.get_object('rad_mode').get_group()[
            int(not custom_sound)].set_active(True)
        self.builder.get_object('filechooser').set_sensitive(custom_sound)
        if self.soundfile:
            self.builder.get_object('filechooser').set_filename(self.soundfile)
        self.builder.get_object('scl_volume').set_value(float(self.volume))
        self.builder.connect_signals({
            'on_mode_changed': self.on_mode_changed,
            'on_file_set': self.on_file_set,
            'on_test_clicked': self.on_test_clicked,
            'on_volume_changed': self.on_volume_changed})

    def save_settings(self):
        self.config['soundfile'] = self.soundfile
        self.config['volume'] = self.volume

    def on_mode_changed(self, radio):
        for idx, single_radio in enumerate(radio.get_group()):
            if not single_radio.get_active():
                filechooser = self.builder.get_object('filechooser')
                filechooser.set_sensitive(idx)
                self.soundfile = filechooser.get_filename() if idx else ''
                break

    def on_file_set(self, filechooser):
        self.soundfile = filechooser.get_filename()

    def on_volume_changed(self, scale):
        self.volume = round(scale.get_value(), 2)

    def on_test_clicked(self, unused_button):
        gstplay(self.get_valid(self.soundfile), self.volume)

    @staticmethod
    def get_valid(soundfile):
        if not soundfile:
            return get_data_file(f'{PACKAGE_NAME}.ogg')
        if not os.path.isfile(soundfile):
            logging.warning(f'Invalid sound file {soundfile}, using default')
            return get_data_file(f'{PACKAGE_NAME}.ogg')
        return soundfile


class _GstPlayThread(threading.Thread):
    def __init__(self, ply):
        self.ply = ply
        threading.Thread.__init__(self)

    def run(self):
        def on_eos(unused_bus, unused_msg):
            self.ply.set_state(Gst.State.NULL)
            return True
        bus = self.ply.get_bus()
        bus.add_signal_watch()
        bus.connect('message::eos', on_eos)
        self.ply.set_state(Gst.State.PLAYING)

_GST_INITIALIZED = False

def gstplay(filename, volume):
    global _GST_INITIALIZED  # pylint: disable=global-statement
    if not _GST_INITIALIZED:
        Gst.init(None)
        _GST_INITIALIZED = True
    try:
        ply = Gst.ElementFactory.make("playbin", "player")
        ply.set_property("uri", "file://" + os.path.abspath(filename))
        ply.set_property("volume", volume)
        playthread = _GstPlayThread(ply)
        playthread.start()
    except Exception:   # pylint: disable=W0703
        logging.exception('', exc_info=True)
