# Copyright 2013, 2014 Patrick Ulbrich <zulu99@gmx.net>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

import os
import importlib
import importlib.util
import inspect
import logging

from functools import reduce

from .utils import get_plugins_path, get_data_file
from .i18n import _


class Plugin:
    """ Plugin base class """
    MANIFEST = (_("Unknown plugin"), _("Empty description"), "0.0",
                _("Unknown author"))
    DEFAULT_CONFIG = None

    def __new__(cls):
        cls._name = None
        cls._service = None
        cls.main_config = None
        cls.config = {}
        cls.ui_file = None
        cls.error_message = None
        cls.builder = None
        cls.actions = {}
        return super().__new__(cls)

    @property
    def human_name(self):
        return self.MANIFEST[0]

    @property
    def available(self):
        """ Plugins can report missing dependencies by returning False """
        return True

    def enable(self):  # pylint: disable=R0201
        """ Plugins can set up things here before getting enabled """
        return

    def disable(self):  # pylint: disable=R0201
        """ Plugins can set up things here before getting disabled """
        return

    def load_settings(self):  # pylint: disable=R0201
        """ Plugins can set the values from config file
            to the builder object
        """
        return

    def save_settings(self):  # pylint: disable=R0201
        """ Plugins can get the config values from the builder
            object
        """
        return

    # HOOKS
    def on_config_saved(self, config):  # pylint: disable=R0201
        """ Plugins can register to 'on_config_saved' hook overwriting this
            method
        """
        return config

    def on_collect(self, accounts):  # pylint: disable=R0201
        """ Plugins can register to 'on_collect' hook overwriting this
            method
        """
        return accounts

    def on_update(self, mails):  # pylint: disable=R0201
        """ Plugins can register to 'on_collect' hook overwriting this
            method
        """
        return mails

    def _load(self, service=None, config=None, ui_file=None):
        self.ui_file = ui_file
        self.main_config = service.config if service else config
        if service:
            self._service = service
            self.actions = {'refresh': service.refresh,
                            'dismiss': service.cache.dismiss,
                            'dismiss_all': service.cache.dismiss_all}
        if not self.DEFAULT_CONFIG or not isinstance(self.DEFAULT_CONFIG, dict):
            return self.available
        self.config = {}
        # try to load plugin config
        if self.main_config.has_section(self._name):
            for name, value in self.main_config.items(self._name):
                self.config[name] = value
        # sync with default config
        for c_key, c_value in self.DEFAULT_CONFIG.items():
            if c_key not in self.config:
                self.config[c_key] = c_value
        return self.available

    def save(self, builder=None):
        logging.debug(f'Config from plugin: {self.config}')
        if builder:
            self.save_settings()
        if not self.main_config.has_section(self._name):
            self.main_config.add_section(self._name)
        for config_key, value in self.config.items():
            self.main_config.set(self._name, config_key, value)
        self.main_config.save()

# pylint: disable=W0212
class PluginAgregator(dict):
    def __init__(self, service=None, config=None):
        assert service or config, 'Provide one of: service, config !!!'
        super().__init__()
        self.service = service
        self.config = self.service.config if service else config
        self.loaded = []
        self.ui_files = {}
        self.error = []
        self.enabled = None
        self._populate()
        self._load()

    def list(self):
        return self.config.get_core(self.config.PLUGINS_ORDER, array=True)

    def enable(self):
        self.enabled = []
        if self and not self.loaded:
            logging.warning('Trying to enable plugins that are not loaded !')
            return
        enabled_plugins = self.config.get_core(self.config.ENABLED_PLUGINS,
                                               array=True)
        for plug_name, plugin in self.items():
            if plug_name not in enabled_plugins:
                continue
            if plug_name in self.error:
                logging.error(f'Error in plugin {plug_name}')
                continue
            try:
                plugin.enable()
            except Exception:
                logging.exception(f'Enabling plugin {plug_name}', exc_info=True)
            else:
                self.enabled.append(plugin)

    def disable(self):
        for plugin in self.enabled:
            try:
                plugin.disable()
            except Exception:  # pylint: disable=W0703
                logging.exception(f'Disabling plugin {plugin.name}',
                                  exc_info=True)
            else:
                self.enabled.remove(plugin)

    def reload(self):
        self.disable()
        self.enable()
        for plug_name in self.loaded:
            self[plug_name]._load(service=self.service,
                                  ui_file=self._get_ui_file(plug_name))

    def _get_ui_file(self, plug_name):
        return self.ui_files[plug_name] if plug_name in self.ui_files else None

    def _populate(self):
        """ Search for plugin in paths """
        p_path = get_plugins_path()
        if not p_path:
            logging.warning(f'No potential plugins path found')
            return
        file_list = list(map(lambda p: list(map(lambda f: os.path.join(p, f),
                                                os.listdir(p))), p_path))
        if not file_list:
            logging.warning(f'No potential plugins found in {p_path}')
            return
        file_list = reduce(lambda x, y: x+y, file_list)
        file_list = filter(lambda f: os.path.splitext(f)[1] == '.py'
                           and '__init__' not in f, file_list)
        plugin_module = None
        for pyfile in file_list:
            plug_name = os.path.splitext(os.path.split(pyfile)[-1])[0]
            if plug_name in self:
                logging.warning(f'Plugin {plug_name} already loaded !')
                continue
            spec = importlib.util.spec_from_file_location(plug_name, pyfile)
            plugin_module = importlib.util.module_from_spec(spec)
            try:
                spec.loader.exec_module(plugin_module)
            except Exception:  # pylint: disable=W0703
                logging.exception(f'Loading {plug_name}', exc_info=True)
                continue
            module_attributes = map(lambda m: getattr(plugin_module, m),
                                    dir(plugin_module))
            module_classes = filter(inspect.isclass, module_attributes)
            plugin_class = list(filter(
                lambda c: c.__name__ != Plugin.__name__
                and issubclass(c, Plugin), module_classes))
            if len(plugin_class) != 1:
                continue
            self[plug_name] = plugin_class[0]
            # UI files
            ui_file = f'{os.path.splitext(pyfile)[0]}.ui'
            if not os.path.exists(ui_file):
                ui_file = get_data_file(
                    f'{os.path.join("plugins", plug_name)}.ui')
            if ui_file:
                self.ui_files[plug_name] = ui_file
        # Plugin ordering
        ordered_map = self.list()
        new_ordered_map = ordered_map.copy()
        for plug_name in self.keys():
            if plug_name not in ordered_map:
                logging.debug(f'Adding {plug_name} to ordered list')
                new_ordered_map.append(plug_name)
        for plug_name in ordered_map:
            if not plug_name in self.keys():
                logging.debug(f'Removing {plug_name} from ordered list')
                new_ordered_map.remove(plug_name)
        if new_ordered_map != ordered_map:
            self.config.set_core(self.config.PLUGINS_ORDER, new_ordered_map)

    def _load(self):
        if not self:
            logging.debug('Load plugins: list is empty')
        for plugin_name in self:
            try:
                self[plugin_name] = self[plugin_name]()
                self[plugin_name]._name = plugin_name
                error = not self[plugin_name]._load(
                    service=self.service, config=self.config,
                    ui_file=self._get_ui_file(plugin_name))
            except Exception:
                logging.exception(f'Loading plugin {plugin_name}',
                                  exc_info=True)
                self.error.append(plugin_name)
            else:
                if error:
                    logging.error(f'Loading plugin {plugin_name}')
                    self.error.append(plugin_name)
                    continue
                self.loaded.append(plugin_name)

    def run_hook(self, hook, args):
        def run_plugin_hook(plugin, args):
            try:
                p_args = getattr(plugin, hook)(args)
            except (TypeError, AttributeError):
                logging.exception(f'Running {plugin._name} hook {hook}',
                                  exc_info=True)
                return args
            if args and not p_args:
                logging.error(
                    f'{plugin._name}({hook}): bad response {p_args}')
                return args
            return p_args

        if not self.enabled:
            return args

        for plug_name in self.list():
            if plug_name not in map(lambda p: p._name, self.enabled):
                continue
            logging.debug(f'Running {hook} hook for {plug_name}')
            args = run_plugin_hook(self[plug_name], args)
        return args
