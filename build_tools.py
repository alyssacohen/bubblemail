import os
import glob
from shutil import which
from PIL import Image, ImageDraw, ImageFont

from bubblemail.config import PACKAGE_NAME

BUILD_DIR = 'build'
BUILD_LOCALE_DIR = os.path.join(BUILD_DIR, 'locale')
BUILD_BIN_DIR = os.path.join(BUILD_DIR, 'bin')
# PLUGIN_DIR = os.path.join(get_python_lib(), f'{PACKAGE_NAME}/plugins')
AVATAR_PROVIDER_CMD = f'{BUILD_BIN_DIR}/{PACKAGE_NAME}-avatar-provider'

def build_avatar_provider(force=True):
    if os.path.isfile(AVATAR_PROVIDER_CMD) and not force:
        return
    if not which('valac'):
        print('No vala compiler found, avatar feature will be unavailable')
        return
    print(f'Building avatar provider')
    if not os.path.exists(BUILD_BIN_DIR):
        os.makedirs(BUILD_BIN_DIR)
    os.system(f'valac -g --pkg folks data/{PACKAGE_NAME}-avatar-provider.vala '
              + f'-d {BUILD_BIN_DIR}')

def create_default_avatars(force=True):
    bg_color_map = {'A':(75, 150, 225), 'B':(0, 75, 0), 'C':(75, 0, 0),
                    'D':(0, 0, 150), 'E':(0, 150, 0), 'F':(150, 0, 0),
                    'G':(0, 0, 225), 'H':(150, 75, 150), 'I':(225, 0, 0),
                    'J':(0, 75, 75), 'K':(75, 75, 0), 'L':(75, 0, 75),
                    'M':(0, 150, 125), 'N':(150, 150, 0), 'O':(150, 0, 150),
                    'P':(0, 225, 225), 'Q':(225, 150, 0), 'R':(225, 0, 225),
                    'S':(225, 150, 75), 'T':(75, 75, 150), 'U':(75, 150, 75),
                    'V':(0, 225, 0), 'W':(150, 150, 150), 'X':(75, 150, 150),
                    'Y':(75, 75, 75), 'Z':(0, 0, 75)}
    size = (48, 48)
    folder = os.path.join('data', 'avatars')
    if not os.path.exists(folder):
        os.makedirs(folder)
    for char, bg_color in bg_color_map.items():
        avatar_file = os.path.join(folder, f'{char}.png')
        if os.path.isfile(avatar_file) and not force:
            continue
        avatar = Image.new('RGB', size, color=bg_color)
        font = ImageFont.truetype(os.path.join('data', 'VeraBd.ttf'), 34)
        draw = ImageDraw.Draw(avatar)
        text_size = font.getmask(char).size
        text_x = int((size[0] - text_size[0])/2)
        text_y = int((size[1] - text_size[1])/4)
        if char in ('A', 'D', 'H', 'S', 'T', 'X', 'Y'):
            text_x += 1
        if char == 'J':
            text_x += 5
            text_y -= 3
        draw.text((text_x, text_y), char, font=font, fill=(255, 255, 255))
        avatar.save(avatar_file)

def gen_locales(force=True):
    for po_file in glob.glob('./po/*.po'):
        lang = os.path.splitext(os.path.basename(po_file))[0]
        dest_path = os.path.join(BUILD_LOCALE_DIR, lang, 'LC_MESSAGES')
        if not os.path.exists(dest_path):
            os.makedirs(dest_path)
        mo_file = os.path.join(dest_path, f'{PACKAGE_NAME}.mo')
        if os.path.isfile(mo_file) and not force:
            continue
        print(f'Creating {mo_file}')
        os.system(f'msgfmt -o {mo_file} {po_file}')
