# Bubblemail

# An extensible mail notification service

### **Please visit [the project homepage](http://bubblemail.free.fr) for full description, screenshots, and packages download**

# Table of Contents
 * [Project description](#prjdesc)
 * [Dependencies](#depends)
 * [Installation instructions](#install)
 * [Usage](#usage)
     * [Mail collect interval](#collival)
     * [Adding Imap/pop accounts](#addacc)
     * [Gnome online accounts](#goacc")
     * [Plugins Tab](#plugtab)
 * [Contribute to the main project](CONTRIBUTING.md#contribute)
     * [Report bugs !](CONTRIBUTING.md#reportbugs)
     * [Translate](CONTRIBUTING.md#translate)
     * [Contribute to the code](CONTRIBUTING.md#contributecode)
 * [Writing Plugins](CONTRIBUTING.md#plugwrite)
     * [General concepts](CONTRIBUTING.md#pluggene)
     * [Hooks](CONTRIBUTING.md#plughooks)
     * [Actions](CONTRIBUTING.md#plugactions)
     * [List and description of protected methods and properties](CONTRIBUTING.md#plugprops)
     * [Anatomy of an existing plugin](CONTRIBUTING.md#pluganat)
 * [Description of the dbus methods and signals](CONTRIBUTING.md#dbus)
     * [Signals](CONTRIBUTING.md#dbussig)
     * [Methods](CONTRIBUTING.md#dbusmet)      
  * [Packaging for distributions](#packaging)

## Project description<a name="prjdesc"></a>

Bubblemail is a [DBus](https://en.wikipedia.org/wiki/D-Bus) service providing a list of the new and unread user's mail from:

 * Local inbox (mbox and maildir formats)
 * POP3 and/or IMAP servers defined as internal accounts
 * Gnome online accounts

It's desktop independent, highly configurable, can be extended via plugins, and is mostly written in python.

Currently, two different frontends can be used to provide user notifications:

  * Notifications using libnotify, implemented directly in the project as a plugin
  * [The gnome desktop specific indicator](https://framagit.org/razer/bubblemail-gnome-shell)

Bubblemail provides a set of default plugins for:

 * Sound notifications
 * Script execution
 * Spam filtering
 * Libnotify notifications

If you're interested in how plugins are made, you can look at the [Plugin Developement Guide](https://framagit.org/razer/bubblemail/blob/master/CONTRIBUTING.md#plugwrite) on the [Contributing page](https://framagit.org/razer/bubblemail/blob/master/CONTRIBUTING.md)

This project started as a fork of the Mailnag project. Since then, almost all the code has been rewritten, but all the references to the [Mailnag's original developers](https://framagit.org/razer/bubblemail/blob/master/AUTHORS) have been kept.

## Dependencies<a name="depends"></a>

### Build
*  **python >= 3.5**
*  [python-setuptools](https://github.com/pypa/setuptools)
*  [python-pillow](https://python-pillow.org/)
*  [vala](https://wiki.gnome.org/Projects/Vala/Documentation)
*  [folks](https://wiki.gnome.org/Projects/Folks)

### Usage
*  **python >= 3.5**
*  [python-distutils](https://docs.python.org/3/library/distutils.html)
*  [python-dbus](https://www.freedesktop.org/wiki/Software/dbus/)
*  [python-gobject](https://pygobject.readthedocs.io/en/latest/)
*  [libsecret](https://wiki.gnome.org/Projects/Libsecret)
*  [gnome-keyring](https://wiki.gnome.org/Projects/GnomeKeyring)

### Optional dependencies

* [gnome-online-accounts](https://wiki.gnome.org/Projects/GnomeOnlineAccounts) (needed for two-factor authentification / oauth2 providers support)
* [folks](https://wiki.gnome.org/Projects/Folks) used for getting avatar pictures

## Installation instructions<a name="install"></a>

Bubblemail uses [setuptools](https://pypi.org/project/setuptools/).

To install it from this repo:

```sh
git clone https://framagit.org/razer/bubblemail.git
cd bubblemail/
sudo python3 setup.py install
```

The installation default prefix `/usr` can be changed with the `--prefix` command line option:

```sh
git clone https://framagit.org/razer/bubblemail.git
cd bubblemail/
sudo python3 setup.py install --prefix=/usr/local
```

For package generation, installation root folder can be provided with the `--root` command line option:

```sh
git clone https://framagit.org/razer/bubblemail.git
cd bubblemail/
python3 setup.py install --root=/tmp/pkg
```

## Usage<a name="usage"></a>

Bubblemail comes in two parts:

 * A service, loaded on desktop login, collects the mails and provides the dbus service
 * A settings interface, using Gtk, providing the user's frontend.

Bubblemail service can be run from the command line:
```sh
bubblemaild
```

Bubblemail settings interface can be run from the command line:
```sh
bubblemail
```

Note: running settings (ie: using `bubblemail` command) will try to get the service running if it's not.

### Mail collect interval<a name="collival"></a>
The interval between accounts checking can be changed in the "General" Tab, and be set to "never" to disable checks

### Adding Imap/pop3 accounts<a name="addacc"></a>
 Multiple imap/pop3 accounts can be added using the account settings window:

* Name: Name of the account
* Type: Imap or Pop3
* Server: url of the imap/pop3 server
* Custom TCP port : an optional port to use with the account
* Folder selection (only Imap) : selection of folders to query for new emails. Default is Inbox.

**Oauth2 / Two-factor identification is currently only supported using gnome online accounts**. Most email providers offer an option to disable oauth2. Please follow your provider specific method in order to use bubblemail without using goa.

### Gnome online accounts<a name="goacc"></a>
Bubblemail periodicaly checks for goa accounts, and syncs its accounts following user changes. Folders can be selected in the account editing window.
### Local mail
Bubblemail checks the presence of a user's mbox file or Maildir folder, and adds local mail options in the settings interface accordingly:

* Folders button (Maildir only): selection of folders to query for new emails. Default is Inbox.
* Switch to enable/disable local mail

### Plugins Tab<a name="plugtab"></a>

Plugins can be enabled / disabled using the plugins tab. Plugin options can be changed for the ones providing it.
Bubblemail checks for plugins:

* In the user's config folder: ~/.config/bubblemail/plugins
* In the system-wide python site-packages installation folder

## Packaging for distributions<a name="packaging"></a>
Generate RPM (Fedora, Redhat Enterprise):
```sh
python3 setup.py bdist_rpm
```
Generate Debian package (Ubuntu, Mint):
```sh
python3 setup.py --command-packages=stdeb.command sdist_dsc
cd deb_dist/bubblemail-<version>
cp ../../packaging/debian/control ../../packaging/debian/copyright debian/
debuild -i -us -uc -b
```
