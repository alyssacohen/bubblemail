#!/usr/bin/env python3
from distutils.core import setup
from distutils.command.install_data import install_data
from distutils.command.build import build
import os
import glob

from bubblemail.config import PACKAGE_NAME, APP_VERSION, APP_URL, APP_DESC
from build_tools import (BUILD_BIN_DIR, BUILD_LOCALE_DIR, build_avatar_provider,
                         create_default_avatars, gen_locales)


class BuildData(build):
    def run(self):
        os.makedirs(BUILD_BIN_DIR, exist_ok=True)
        # generate translations
        gen_locales()
        build.run(self)
        build_avatar_provider()
        create_default_avatars()
        avatar_provider_bin = os.path.join(BUILD_BIN_DIR,
                                           f'{PACKAGE_NAME}-avatar-provider')
        if os.path.isfile(avatar_provider_bin):
            return
        with open(avatar_provider_bin, 'w') as fake_provider:
            fake_provider.write('#!/bin/bash\n')
        os.chmod(avatar_provider_bin, 0o755)


class InstallData(install_data):
    def add_data_files(self, data_files, walk_path, root_path, dst_path):
        for data_file in data_files:
            src_path = os.path.join(root_path, data_file)
            _dst_path = os.path.join(
                dst_path, os.path.dirname(src_path[len(walk_path)+1:]))
            self.data_files.append((_dst_path, [src_path]))

    def run(self):
        # Install translation po files
        for root_path, _, files in os.walk(BUILD_LOCALE_DIR):
            self.add_data_files(files, BUILD_LOCALE_DIR, root_path,
                                'share/locale')
        # Install  icons
        for root_path, _, files in os.walk('data/icons'):
            self.add_data_files(files, 'data/icons', root_path, 'share/icons')
        install_data.run(self)


setup(
    name=PACKAGE_NAME,
    version=APP_VERSION,
    description=APP_DESC,
    url=APP_URL,
    long_description=(
        'Bubblemail is a DBus service providing a list of the new and unread '
        + 'user\'s mail from local mailboxes, pop, imap, and gnome online '
        + 'accounts. It includes a libnotify frontend to create notifications '
        + 'and can be used by other frontends as well.'),
    author='Razer',
    author_email='razerraz@free.fr',
    license='LGPL-2.0-or-later',
    python_requires='>=3.5',
    classifiers=[
        'Topic :: Communications :: Email',
        'Development Status :: 5 - Production/Stable',
        'Environment :: X11 Applications :: GTK',
        'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',  #pylint: disable=line-too-long
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='mail gnome account notification dbus gtk',
    packages=[PACKAGE_NAME, f'{PACKAGE_NAME}.ui', f'{PACKAGE_NAME}.plugins'],
    scripts=[f'bin/{PACKAGE_NAME}d', f'bin/{PACKAGE_NAME}'],
    data_files=[
        (f'share/{PACKAGE_NAME}/plugins', glob.glob(f'data/plugins/*.ui')),
        ('bin',
         [os.path.join(BUILD_BIN_DIR, f'{PACKAGE_NAME}-avatar-provider')]),
        (f'share/{PACKAGE_NAME}',
         glob.glob('data/*.ui') + glob.glob('data/*.css')),
        (f'share/{PACKAGE_NAME}/avatars', glob.glob('data/avatars/*.png')),
        (f'share/{PACKAGE_NAME}',
         [f'data/{PACKAGE_NAME}.ogg'] + glob.glob('data/*.png')),
        ('share/metainfo', [f'data/{PACKAGE_NAME}.appdata.xml']),
        ('share/applications', [f'data/{PACKAGE_NAME}.desktop']),
        ('/etc/xdg/autostart', [f'data/{PACKAGE_NAME}d.desktop'])
        ],
    cmdclass={
        'build': BuildData,
        'install_data': InstallData}
    )
