# Copyright 2019 - 2020 razer <razerraz@free.fr>
# Copyright 2016, 2018 Timo Kankare <timo.kankare@iki.fi>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

"""Test cases for Account."""
# TODO: GOA tests
import os
import sys
import shutil
import pytest

sys.path.insert(0, '../bubblemail')
# pylint: disable=C0413
from bubblemail.config import Config, FAKESPACE
from bubblemail.account import (Account as A, RemoteAccount as RA,
                                LocalAccount as LA)
from bubblemail.localmail import LocalMail as LM

# pylint: disable=W0621
@pytest.fixture
def fake_config_folder():
    if os.path.isdir('/tmp/bubblemail-test'):
        shutil.rmtree('/tmp/bubblemail-test')
    return '/tmp/bubblemail-test'

class FakeSecretStore:
    def save(self, account_name, uuid, password):
        pass

    def password(self, uuid):  # pylint: disable=R0201,W0613
        return 'secret'

    def clear(self, uuid):
        pass

@pytest.fixture
def fake_secret_store():
    return FakeSecretStore

def test_default_values(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config)
    assert len(account[A.UUID]) >= 8 and account.section and account[A.ENABLED]
    assert account[A.TYPE] == A.INTERNAL
    local_account = LA(config)
    assert local_account[A.BACKEND] == A.MAILDIR
    assert local_account[A.FOLDERS] == []
    remote_account = RA(config)
    assert remote_account[A.BACKEND] == A.IMAP
    assert remote_account[A.FOLDERS] == []

def test_null_folder_filter(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config, folders=['a', '', 'def'])
    assert account[A.FOLDERS] == ['a', 'def']

def test_account_folder_list_from_string(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config, folders=f'a,r, f{FAKESPACE}with{FAKESPACE}s, c, def')
    assert account[A.FOLDERS] == ['a,r', 'f with s', 'c', 'def']

def test_account_config_section_name(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config, uuid='12345678')
    assert account.section == 'Account 12345678'

def test_account_uuid_is_unique(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account_map = [A(config, name='a', user='x', server='xx'),
                   A(config, name='b', backend=A.POP3, user='x', server='xx'),
                   A(config, name='c', user='x', server='xx')]
    assert len(set(a[A.UUID] for a in account_map)) == len(account_map)

def test_account_validation(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    local_account = LA(config)
    assert not local_account.is_valid
    local_account[A.NAME] = 'valid account'
    local_account[LA.PATH] = '/tmp/Maildir'
    assert not local_account.is_valid
    os.makedirs(local_account[LA.PATH])
    assert local_account.is_valid
    os.rmdir(local_account[LA.PATH])
    open(local_account[LA.PATH], 'a').close()
    assert not local_account.is_valid
    local_account[LA.BACKEND] = A.MBOX
    assert local_account.is_valid
    for key in A.NAME, LA.PATH:
        valid_value = local_account[key]
        local_account[key] = ''
        assert not local_account.is_valid
        local_account[key] = valid_value
    assert local_account.is_valid
    os.remove(local_account[LA.PATH])
    local_account[A.BACKEND] = 'unsupportedBackend'
    assert not local_account.is_valid
    remote_account = RA(config)
    assert not remote_account.is_valid
    remote_account[A.NAME] = 'valid account'
    remote_account[RA.SERVER] = 'imap.valid.server.org'
    remote_account[RA.USER] = 'valid username'
    remote_account[RA.PASS] = 'validpassword'
    assert remote_account.is_valid
    for key in A.NAME, RA.SERVER, RA.USER, RA.PASS:
        valid_value = remote_account[key]
        remote_account[key] = ''
        assert not remote_account.is_valid
        remote_account[key] = valid_value
    assert remote_account.is_valid
    remote_account[A.BACKEND] = 'unsupportedBackend'
    assert not remote_account.is_valid

def test_account_keep_configuration_across_save(mocker, fake_config_folder,
                                                fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    account_template = dict(
        name='my name', folders=['a', 'b'], type=A.INTERNAL, backend=A.MAILDIR,
        path='/home/user/Maildir', enabled='0')
    local_account = LA(config)
    for key, value in account_template.items():
        local_account[key] = value
    local_account.save()
    config.load()
    saved_account = LA.get(config, local_account[A.UUID])
    for key, value in account_template.items():
        assert saved_account[key] == value

    account_template = dict(
        name='my name', user='who', password='secret', server='example.org',
        port='124', folders=['a', 'b'], type=A.INTERNAL, backend=A.IMAP,
        enabled='0')
    remote_account = RA(config)
    for key, value in account_template.items():
        remote_account[key] = value
    remote_account.save()
    config.load()
    saved_account = RA.get(config, remote_account[A.UUID])
    for key, value in account_template.items():
        assert saved_account[key] == value

def test_account_list(mocker, fake_config_folder, fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    account_map = [LA(config, name='a', bavkend=A.MBOX, path='/home/user/.mbx'),
                   RA(config, name='b', backend=A.POP3, user='x', server='xx'),
                   RA(config, name='c', user='x', server='xx')]
    for account in account_map:
        account.save()
    assert list(A.list(config)) == account_map

def test_account_delete(mocker, fake_config_folder, fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    remote_account = RA(config)
    account_uuid = remote_account[A.UUID]
    remote_account.save()
    remote_account = A.get(config, account_uuid)
    remote_account.delete()
    assert not A.get(config, account_uuid)

def test_account_set_status(mocker, fake_config_folder, fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    account_template = dict(
        name='my name', folders=['a', 'b'], type=A.INTERNAL, backend=A.MAILDIR,
        path='/home/user/Maildir', enabled='0')
    local_account = LA(config)
    for key, value in account_template.items():
        local_account[key] = value
    local_account.set_status(LM.ERROR, LM.INVALID_ERR)
    # Account should not be created on set_status call
    assert not LA.get(config, local_account[A.UUID])
    local_account = LA(config)
    for key, value in account_template.items():
        local_account[key] = value
    local_account.save()
    local_account.set_status(LM.ERROR, LM.INVALID_ERR)
    config.load()
    saved_account = LA.get(config, local_account[A.UUID])
    assert saved_account[A.STATUS] == str(LM.ERROR)
    assert saved_account[A.ERROR] == LM.INVALID_ERR
    saved_account.set_status(LM.SUCCESS)
    config.load()
    saved_account = LA.get(config, local_account[A.UUID])
    assert saved_account[A.STATUS] == str(LM.SUCCESS)
    assert not saved_account[A.ERROR]
