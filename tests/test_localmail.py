# Copyright 2019 - 2020 razer <razerraz@free.fr>
# Copyright 2016, 2018 Timo Kankare <timo.kankare@iki.fi>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

""" Test cases for LocalMail module """
import os
import sys
import tarfile
import shutil
import pytest

sys.path.insert(0, '../bubblemail')
# pylint: disable=C0413, C0415, W0621, W0212, R0914
from bubblemail.config import Config as C
from bubblemail.account import Account as A, LocalAccount as LA
from bubblemail.localmail import LocalMail as LM

CACHE_DIR = '/tmp/bubblemail-test'
FAKE_HOME = '/tmp/bb_test_fake_home'

@pytest.fixture
def fake_config_folder():
    if os.path.isdir(CACHE_DIR):
        shutil.rmtree(CACHE_DIR)
    return CACHE_DIR

def test_localmail(monkeypatch, mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    if os.path.exists(FAKE_HOME):
        shutil.rmtree(FAKE_HOME)
    fake_maildir = os.path.join(FAKE_HOME, 'fake_mail')
    os.makedirs(fake_maildir)
    monkeypatch.setenv('HOME', FAKE_HOME)
    for fake_localmail in 'fake_maildir', 'fake_mbox':
        with tarfile.open(f'tests/{fake_localmail}.tar.xz') as tar_file:
            tar_file.extractall(fake_maildir)
    config = C()
    account = LA(config, name='a', path=os.path.join(fake_maildir, 'Maildir'))
    account_uuid = account[A.UUID]
    account[A.ERROR] = 'account error'
    account.save(create=True)
    invalid_account_mail = LM('invalid')
    assert not invalid_account_mail.available
    account_mail = LM(account)
    assert account_mail.available
    assert not account[A.ERROR]
    saved_account = A.get(config, account_uuid)
    assert saved_account
    assert not saved_account[A.ERROR]
    assert account_mail.is_browsable
    account[A.BACKEND] = A.MBOX
    assert not account_mail.is_browsable
    assert not account_mail.available
    assert account[A.ERROR]
    assert A.get(config, account_uuid)[A.ERROR]
    account[LA.PATH] = os.path.join(fake_maildir, '.mbox')
    assert not account_mail.is_browsable
    assert account_mail.available
    assert not account[A.ERROR]
    assert not A.get(config, account_uuid)[A.ERROR]
    account[A.BACKEND] = A.MAILDIR
    assert not account_mail.available
    assert account[A.ERROR]
