# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

""" Test cases for Plugin and PluginAgregator classes """
import os
import sys
import shutil
import email
import pytest

sys.path.insert(0, '../bubblemail')
# pylint: disable=C0413, W0621, W0212, W0231, W0233, R0903, R0914
from bubblemail.config import Config as C
from bubblemail.mail import Mail as M
from bubblemail.account import Account as A, RemoteAccount as RA
from bubblemail.plugin import Plugin, PluginAgregator

CONF_DIR = '/tmp/bubblemail-test'
MAIL_STRING = """\
From: You <you@example.org>
To: Me <me@example.org>
Subject: Hello...
Date: Tue, 01 May 2018 16:28:08 +0300

...World!
"""

@pytest.fixture
def fake_config_folder():
    if os.path.isdir(CONF_DIR):
        shutil.rmtree(CONF_DIR)
    return CONF_DIR

class InvalidTestPlugin(Plugin):  # pylint: disable=W0223
    MANIFEST = (
        "InvalidTestPlugin", "This plugin should not work",
        "0.1", "Razer <razerraz@free.fr")
    DEFAULT_CONFIG = {'my_valid_prop': 'valid'}

    @property
    def available(self):
        return False

    def enable(self):
        assert False

    def disable(self):
        assert False

    def on_collect(self, accounts):  # pylint: disable=W0613
        assert False

class ValidTestPlugin(Plugin):
    MANIFEST = (
        "ValidTestPlugin", "This plugin should work",
        "0.1", "Razer <razerraz@free.fr")
    DEFAULT_CONFIG = {'my_valid_prop': 'valid'}

    def __init__(self):
        super().__init__()
        self.state = dict(enabled=False, disabled=False, settings_loaded=False,
                          settings_saved=False, collect_hook=False,
                          update_hook=False, config_saved_hook=False)
        self.got_disabled = False

    def enable(self):
        self.state['enabled'] = True

    def disable(self):
        self.state['disabled'] = True

    def load_settings(self):
        self.state['settings_loaded'] = True
        assert self.config['my_valid_prop'] == 'valid'

    def save_settings(self):
        self.state['settings_saved'] = True
        self.config['my_valid_prop'] = 'saved'

    def on_config_saved(self, config):
        self.state['config_saved_hook'] = config
        return config

    def on_collect(self, accounts):
        self.state['collect_hook'] = accounts
        return accounts

    def on_update(self, mails):
        # new_mails, unseen_mails = mails
        self.state['update_hook'] = mails
        return mails

class FakeCache(dict):
    def dismiss(self, uuid):
        self[uuid] = 'dismissed'

    def dismiss_all(self, uuid):
        self[uuid] = 'dismissed'

class FakeService:
    def __init__(self, config):
        self.config = config
        self.plugins = None
        self.refreshed = False
        self.cache = FakeCache()
        self.got_shutdown_request = False
        self.handler = {'shutdown_request': self.shutdown_request}
    def shutdown_request(self):
        self.got_shutdown_request = True
    def refresh(self):
        self.refreshed = True

class UnpopulatedPluginAgregator(PluginAgregator):
    def _populate(self):
        pass

def test_valid_plugin_enable_disable(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    config.set(C.CORE, C.ENABLED_PLUGINS, 'ValidTestPlugin')
    fake_service = FakeService(config)
    plugin_agregator = UnpopulatedPluginAgregator(service=fake_service)
    plugin_agregator['ValidTestPlugin'] = ValidTestPlugin
    plugin_agregator.enable()
    # assert not plugin_agregator.loaded
    assert not plugin_agregator.enabled
    plugin_agregator._load()
    assert plugin_agregator.loaded[0] == 'ValidTestPlugin'
    plugin_agregator.enable()
    assert plugin_agregator.enabled[0] == plugin_agregator['ValidTestPlugin']
    plugin_agregator.error.append('ValidTestPlugin')
    plugin_agregator.enable()
    assert not plugin_agregator.enabled
    plugin_agregator.error.remove('ValidTestPlugin')
    plugin_agregator.enable()
    assert plugin_agregator.enabled[0] == plugin_agregator['ValidTestPlugin']
    assert plugin_agregator['ValidTestPlugin'].state['enabled']
    plugin_agregator.disable()
    assert not plugin_agregator.enabled
    assert plugin_agregator['ValidTestPlugin'].state['disabled']

def test_invalid_plugin_enable(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    config.set(C.CORE, C.ENABLED_PLUGINS, 'InvalidTestPlugin')
    fake_service = FakeService(config)
    plugin_agregator = PluginAgregator(service=fake_service)
    plugin_agregator['InvalidTestPlugin'] = InvalidTestPlugin
    plugin_agregator._load()
    plugin_agregator.enable()
    assert not plugin_agregator.enabled

def test_invalid_plugin_enable_with_available_state(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    config.set(C.CORE, C.ENABLED_PLUGINS, 'InvalidTestPlugin')
    fake_service = FakeService(config)
    plugin_agregator = PluginAgregator(service=fake_service)

    class InvalidTestPluginWithAvalaibleState(InvalidTestPlugin):
        @property
        def available(self):
            return True

    plugin_agregator['InvalidTestPlugin'] = InvalidTestPluginWithAvalaibleState
    plugin_agregator._load()
    plugin_agregator.enable()
    assert not plugin_agregator.enabled

def test_plugin_settings_save(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    config.set_core(C.ENABLED_PLUGINS, ['ValidTestPlugin'])
    # fake_service = FakeService(config)
    plugin_agregator = PluginAgregator(config=config)
    plugin_agregator['ValidTestPlugin'] = ValidTestPlugin
    plugin_agregator._load()
    plugin_agregator['ValidTestPlugin'].save()
    assert not plugin_agregator['ValidTestPlugin'].state['settings_saved']
    plugin_agregator['ValidTestPlugin'].save(builder=True)
    assert plugin_agregator['ValidTestPlugin'].state['settings_saved']
    assert config.get('ValidTestPlugin', 'my_valid_prop') == 'saved'

def get_agregator(config, plug_name, plugin):
    config.set_core(C.ENABLED_PLUGINS, [plug_name])
    config.set_core(C.PLUGINS_ORDER, [plug_name])
    fake_service = FakeService(config)
    plugin_agregator = UnpopulatedPluginAgregator(service=fake_service)
    plugin_agregator[plug_name] = plugin
    plugin_agregator._load()
    plugin_agregator.enable()
    return plugin_agregator

def test_plugin_on_collect_hook(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    RA(config, name='a', user='x', server='xx').save()
    accounts = list(A.list(config))
    plugin_agregator = get_agregator(config, 'ValidTestPlugin', ValidTestPlugin)
    hooked_accounts = plugin_agregator.run_hook('on_collect', accounts)
    assert plugin_agregator['ValidTestPlugin'].state['collect_hook'] == accounts
    assert hooked_accounts == accounts

    class ValidTestPluginMutatingAccounts(ValidTestPlugin):
        def on_collect(self, accounts):  # pylint: disable=W0221
            accounts[0][RA.SERVER] = 'yy'
            return accounts

    config = C()
    RA(config, name='a', user='x', server='xx').save()
    accounts = list(A.list(config))
    plugin_agregator = get_agregator(config, 'ValidTestPluginMutatingAccounts',
                                     ValidTestPluginMutatingAccounts)
    hooked_accounts = plugin_agregator.run_hook('on_collect', accounts)
    assert hooked_accounts[0][RA.SERVER] == 'yy'

def test_invalid_plugin_on_collect_hook(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    RA(config, name='a', user='x', server='xx').save()
    accounts = list(A.list(config))
    plugin_agregator = get_agregator(config, 'InvalidTestPlugin',
                                     InvalidTestPlugin)
    hooked_accounts = plugin_agregator.run_hook('on_collect', accounts)
    assert hooked_accounts == accounts

    class InvalidTestPluginBadFuncArg(InvalidTestPlugin):
        def on_collect(self):  # pylint: disable=W0221
            pass

    config = C()
    RA(config, name='a', user='x', server='xx').save()
    accounts = list(A.list(config))
    plugin_agregator = get_agregator(config, 'InvalidTestPluginBadFuncArg',
                                     InvalidTestPluginBadFuncArg)
    hooked_accounts = plugin_agregator.run_hook('on_collect', accounts)
    assert hooked_accounts == accounts

    class InvalidTestPluginBadReturnVal(InvalidTestPlugin):
        def on_collect(self, accounts):
            return 'toto', None

    config = C()
    RA(config, name='a', user='x', server='xx').save()
    accounts = list(A.list(config))
    plugin_agregator = get_agregator(config, 'InvalidTestPluginBadReturnVal',
                                     InvalidTestPluginBadReturnVal)
    hooked_accounts = plugin_agregator.run_hook('on_collect', accounts)
    assert hooked_accounts == accounts

def test_plugin_on_config_saved_hook(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    plugin_agregator = get_agregator(config, 'ValidTestPlugin', ValidTestPlugin)
    hooked_config = plugin_agregator.run_hook('on_config_saved', config)
    assert plugin_agregator['ValidTestPlugin'].state[
        'config_saved_hook'] == config
    assert hooked_config == config

    class ValidTestPluginMutatingConfig(ValidTestPlugin):
        def on_config_saved(self, config):
            config.set(config.CORE, config.POLL_INTERVAL, '1')
            return config

    config = C()
    plugin_agregator = get_agregator(config, 'ValidTestPluginMutatingConfig',
                                     ValidTestPluginMutatingConfig)
    hooked_config = plugin_agregator.run_hook('on_config_saved', config)
    assert hooked_config.get(C.CORE, C.POLL_INTERVAL) == '1'

def get_fake_mails():
    mail_template1 = email.message_from_string(MAIL_STRING)
    mail_template2 = email.message_from_string(
        MAIL_STRING.replace('Hello', 'Bye'))
    mail1 = M.from_raw('my acc', '', mail_template1)
    mail2 = M.from_raw('my acc2', '', mail_template1)
    mail3 = M.from_raw('my acc3', '', mail_template2)
    mail4 = M.from_raw('my acc4', '', mail_template2)
    new_mails = [mail1, mail2]
    unseen_mails = [mail3, mail4]
    return [new_mails, unseen_mails]

def test_plugin_on_update_hook(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    new_mails, unseen_mails = get_fake_mails()
    config = C()
    plugin_agregator = get_agregator(config, 'ValidTestPlugin', ValidTestPlugin)
    hooked_new_mail, hooked_unseen_mail = plugin_agregator.run_hook(
        'on_update', [new_mails, unseen_mails])
    assert plugin_agregator['ValidTestPlugin'].state[
        'update_hook'] == [new_mails, unseen_mails]
    assert hooked_new_mail == new_mails
    assert hooked_unseen_mail == unseen_mails
    mutated_new_mails = new_mails.copy()
    mutated_new_mails.pop(-1)

    class ValidTestPluginMutatingMails(ValidTestPlugin):
        def on_update(self, mails):
            return [mutated_new_mails, []]

    config = C()
    plugin_agregator = get_agregator(config, 'ValidTestPluginMutatingMails',
                                     ValidTestPluginMutatingMails)
    hooked_new_mail, hooked_unseen_mail = plugin_agregator.run_hook(
        'on_update', [new_mails, unseen_mails])
    assert hooked_new_mail == mutated_new_mails
    assert hooked_unseen_mail == []

def test_invalid_plugin_on_update_hook(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    new_mails, unseen_mails = get_fake_mails()
    config = C()
    plugin_agregator = get_agregator(config, 'InvalidTestPlugin',
                                     InvalidTestPlugin)
    hooked_new_mail, hooked_unseen_mail = plugin_agregator.run_hook(
        'on_update', [new_mails, unseen_mails])
    assert hooked_new_mail == new_mails
    assert hooked_unseen_mail == unseen_mails

    class InvalidTestPluginBadFuncArg(InvalidTestPlugin):
        def on_update(self):  # pylint: disable=W0221
            pass

    config = C()
    plugin_agregator = get_agregator(config, 'InvalidTestPluginBadFuncArg',
                                     InvalidTestPluginBadFuncArg)
    hooked_new_mail, hooked_unseen_mail = plugin_agregator.run_hook(
        'on_update', [new_mails, unseen_mails])
    assert hooked_new_mail == new_mails
    assert hooked_unseen_mail == unseen_mails

    class InvalidTestPluginBadReturnVal(InvalidTestPlugin):
        def on_update(self, mails):
            return 'toto'

    config = C()
    plugin_agregator = get_agregator(config, 'InvalidTestPluginBadReturnVal',
                                     InvalidTestPluginBadReturnVal)
    hooked_new_mail, hooked_unseen_mail = plugin_agregator.run_hook(
        'on_update', [new_mails, unseen_mails])
    assert hooked_new_mail == new_mails
    assert hooked_unseen_mail == unseen_mails
