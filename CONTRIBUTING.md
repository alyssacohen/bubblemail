# Table of Contents
 * [Contribute to the main project](#contribute)
     * [Report bugs!](#reportbugs)
     * [Translate](#translate)
     * [Contribute to the code](#contributecode)
 * [Writing Plugins](#plugwrite)
     * [General concepts](#pluggene)
     * [Hooks](#plughooks)
     * [Actions](#plugactions)
     * [List and description of protected methods and properties](#plugprops)
     * [Anatomy of an existing plugin](#pluganat)
 * [Description of the dbus methods and signals](#dbus)
     * [Signals](#dbussig)
     * [Methods](#dbusmet)
    
# Contribute to the main project<a name="contribute"></a>

All contributors are desired and welcome.

## Report bugs!<a name="reportbugs"></a>

Reporting bugs is often more useful than writing code. Please take the time to add yours if something goes wrong eventually.

## Translate<a name="translate"></a>

Bubblemail uses basic gettext and pot files. Feel free to use the translation file template to PR and add yours.

## Contribute to the code<a name="contributecode"></a>

Bubblemail is a fairly new project, with a lot a of things to improve. The code is clean, pep8 compliant and working on it should be fun, I hope. Feel free to hack it and PR your changes, following some basic rules:
**All PRs must be pylint and pep8 compliant and the provided tests must run successfully**.

To help any developers with the task, Bubblemail components can run without any installation, in development mode:
```sh
python run-dev.py
```

In this case, **logging is sent to the console in debug level**.

Several options are available:

* **`-d`** or **`--service`** (Default)

  Run the Bubblemail service

* **`-s`** or **`--settings`**

  Run the Bubblemail settings gtk interface

* **`-r`** or **`--rebuild`**

  Force rebuild - even if existing binaries are found - needed components (actually vala source providing avatars via libfolks)

* **`-c`** or **`--configfolder`**

  Use given config folder instead of default `~/.config/bubblemail`

# Writing Plugins<a name="plugwrite"></a>

## General concepts<a name="pluggene"></a>

Bubblemail features and behavior can be extended with plugins.

 * Bubblemail searches for plugins in its own module `plugins` folder and in the *xdg standard* config folder `~/.config/bubblemail/plugins`.
 * Bubblemail's plugins have to be **a python file** following the scheme: `myawesomeplugin.py`
 * The plugin file must contain a **python class inherited from Bubblemail's generic "Plugin" class**.
 * The inherited python class **must include a static tuple property named MANIFEST with name, description, version and author references**:

```python
        class MyAwesomePlugin(Plugin):
            MANIFEST = (
                "MyAwesomePlugin", 
                "An awesome plugin",
                "0.1", 
                "An awesome author <myemail@mydomain.com>")
```

 * Optionally, an `<myawesomeplugin>.ui` file can be provided to **add a settings page in the settings interface**. **[Glade](https://glade.gnome.org/) software** should be used to create it. Then, a **set of default config values** for the plugin can be provided with a **static dictionary property named DEFAULT_CONFIG**. The values will be saved and restored internally:

```python
        class MyAwesomePlugin(Plugin):
            MANIFEST = (...)
            DEFAULT_CONFIG = {'my_awesome_option': 100}
```

## Hooks<a name="plughooks"></a>

Three different hooks allow plugins to interface with Bubblemail and change its behavior.

 * **`on_config_saved`** is triggered when **a config value changes** and provides the actual **config** as a dictionary.
 * **`on_collect`** is triggered **before the cyclic mail collect** and provides the **list of accounts**, that are dictionary instances.
 * **`on_update`** is triggered **after the cyclic mail collect** and provides **2 lists**, the first one containing **new mails** and the second one **all mails flagged unseen**, that are dictionary instances.

Plugins can use **hooks** by providing methods sharing the hook's name. As an example, the internal [libnotify](https://framagit.org/razer/bubblemail/blob/master/bubblemail/plugins/libnotifyplugin.py) plugin uses the **on_update** hook:

```python
    class LibNotifyPlugin(Plugin):
    ...
        def on_update(self, new_mails, unseen_mails):
            # Do here what you want to do with mails
            # But do not forget to return all of the hook parameters
            return new_mails, unseen_mails
```

As the comment code says, you have to return the same parameters that the hook receives. It can be perceived as way to do that, using the hook as a sort of middleware, even if Bubblemail prevents things to go wrong if you don't.

## Actions<a name="plugactions"></a>

Actions can be done by plugins at any moment, using the provided main plugin class **actions** dictionary.
3 different actions are currently available:
 * **refresh** to do a **mail collect**
 * **dismiss** to dismiss a specific mail
 * **dismiss_all** to dismiss all mails

```python
        class MyAwesomePlugin(Plugin):
            ...
            def user_wants_to_get_new_mails(self):
                if today_temp > 20:
                    self.actions['refresh']()
```

## List and description of protected methods and properties<a name="plugprops"></a>

### Static properties

 * **`Plugin.MANIFEST`** *(tuple)*:  Plugin name, description, version and author references
 * **`Plugin.DEFAULT_CONFIG`** *(dict/optional)*: Optional config values as pairs of *config_name/value*

### Class properties

 * **`PluginInstance.available`** *(bool/readonly)*: Plugins can report an invalid state, for example if dependencies are missing, setting this property to false
 * **`PluginInstance.actions`** *(dict/readonly)*: Refresh/dismiss/dismiss_all actions as a pair of action name/related method
 * **`PluginInstance.config`** *(dict/readwrite)*: Access to the config values defined in `Plugin.DEFAULT_CONFIG`
 * **`PluginInstance.builder`** *(Builder instance/readonly)*: Access to the plugin part of the gtk builder instance

### Methods

 * **`PluginInstance.enable()`**: reached when the plugin is enabled
 * **`PluginInstance.disable()`**: reached when the plugin is disabled
 * **`PluginInstance.load_settings()`**: reached when the plugin's user interface is loaded
 * **`PluginInstance.save_settings()`**: reached when the plugin's user interface is closed

### Hooks

 * **`PluginInstance.on_config_saved(config)`**
 * **`PluginInstance.on_collect(accounts)`**
 * **`PluginInstance.on_update(new_mails, unseen_mails)`**

## Anatomy of an existing plugin<a name="pluganat"></a>

Bubblemail provides some plugins, for instance a [spam filter](https://framagit.org/razer/bubblemail/blob/master/bubblemail/plugins/spamfilterplugin.py):

```python
    from bubblemail.plugin import Plugin
    from bubblemail.i18n import _
    
    class SpamfilterPlugin(Plugin):
        MANIFEST = (
            _("Spam Filter"), _("Filters out unwanted mails."),
            "1.1.1", "Patrick Ulbrich <zulu99@gmx.net>, Razer <razerraz@free.fr")
        DEFAULT_CONFIG = {'filter_list' : 'newsletter, viagra'}
```

Since the plugin needs some user action on the filter list, the glade file [spamfilterplugin.ui](https://framagit.org/razer/bubblemail/blob/master/bubblemail/plugins/spamfilterplugin.py) is provided.
The load/save process of the config options is done implementing the **load_settings** and **save_settings**
methods:

```python
    ...
    class SpamfilterPlugin(Plugin):
        ...
            @property
            def txt_buffer(self):
                return self.builder.get_object('txt_buffer')
        
            def load_settings(self):
                self.txt_buffer.set_text(self.config['filter_list'])
        
            def save_settings(self):
                start, end = self.txt_buffer.get_bounds()
                self.config['filter_list'] = self.txt_buffer.get_text(start, end, False)
```

The **config** class method provides read/write access to the config values.

The **buider** class method contains the plugin gtk builder instance.

Note: the text_buffer property is only included here for better comprehension of the code.


Finally, the plugin registers to the **on_update** hook in order to filter incoming mails:

```python
    ...
    class SpamfilterPlugin(Plugin):
        ...
            def on_update(self, new_mails, unseen_mails):
                return (list(filter(self.mail_filter, new_mails)),
                        list(filter(self.mail_filter, unseen_mails)))
        
            def mail_filter(self, mail):
                is_spam = False
                for filter_exp in self.filter_list:
                    # remove CR and white space
                    filter_exp = filter_exp.strip()
                    if not filter_exp:
                        continue
                    filter_exp = filter_exp.lower()
                    if filter_exp in (mail[mail.NAME].lower(),
                                      mail[mail.ADDRESS].lower(),
                                      mail[mail.SUBJECT].lower()):
                        # sender or subject contains filter string
                        is_spam = True
                        break
                return not is_spam
```

And that's all!

# Description of the dbus methods and signals<a name="dbus"></a>

Bubblemail is a Dbus service. The methods and signals are used internaly by the GTK interface (running in a different process than the service) and can be used by third party software as well, like the [gnome-shell extension indicator](https://framagit.org/razer/bubblemail-gnome-shell) actualy does.

## Signals<a name="dbussig"></a>

### `ContentUpdate`

* Signature: `aa{ss}`
* Content: List of unread mails
* Trigger: List of collected mails has changed

### `ConfigUpdate`

  * Signature: `aa{ss}`
  * Content: List of config (key/value pairs)
  * Trigger: Config has changed

## Methods<a name="dbusmet"></a>

### `GetConfig` 

  * In signature: none
  * In content: none
  * Out signature: `aa{ss}`
  * Out content: List of config (key/value pairs), that can be converted back to a python dict using the `Config.load_serialized()` method

### `SetConfig`  

  * In signature: `aa{ss}`
  * In content: List of config (key/value pairs), result of the `Config.serialize()` method
  * Out Signature: none
  * Out Content: none

### `Refresh`  

  * In signature: none
  * In content: none
  * Out signature: none
  * Out content: none
  * Action: Run a mail collect

### `GetContent`  

  * In signature: none
  * In content: none
  * Out signature: `aa{ss}`
  * Out content: List of unread mails

### `GetPluginState`  

  * In signature: none
  * In content: none
  * Out signature: `as`
  * Out content: List of plugins causing an error

### `EnablePlugin` 

  * In signature: `s`
  * In content: Name of the plugin to enable
  * Out Signature: none
  * Out Content: none
  * Action: Enable the plugin with the given name

### `DisablePlugin` 

  * In signature: `s`
  * In content: Name of the plugin to disable
  * Out Signature: none
  * Out Content: none
  * Action: Disable the plugin with the given name

### `Dismiss` 
  * In signature: `s`
  * In content: Uuid of the mail to dismiss
  * Out Signature: none
  * Out Content: none
  * Action: Dismiss the mail with the given uuid

### `DismissAll` 
  * In signature: none
  * In content: none
  * Out Signature: none
  * Out Content: none
  * Action: Dismiss all unread mails