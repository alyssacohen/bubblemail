#!/usr/bin/env python3
import os
import builtins
import argparse

builtins.BB_DEVMODE = True
builtins.BB_DEVPATH = os.path.dirname(os.path.realpath(__file__))
PACKAGE_NAME = 'bubblemail'

PARSER = argparse.ArgumentParser(
    prog=PACKAGE_NAME, description=f'Run {PACKAGE_NAME} components in dev mode')
PARSER.add_argument('-d', '--service', action='store_true', default=True,
                    help=f'Start service (default)')
PARSER.add_argument('-s', '--settings', action='store_true',
                    help=f'Show settings window')
PARSER.add_argument('-r', '--rebuild', action='store_true',
                    help=f'Rebuild components')
PARSER.add_argument('-f', '--homefolder',
                    help=f'Folder used to store config & cache (default is ~)')
# PARSER.add_argument('-a', '--cachefolder',
#                     help=f'Cache folder (default is ~/.cache/bubblemail)')

ARGS = PARSER.parse_args()
if ARGS.homefolder:
    print(f'Using home folder {ARGS.homefolder}')
    builtins.BB_DEVHOMEPATH = ARGS.homefolder
# if ARGS.cachefolder:
#     print(f'Using cache folder {ARGS.cachefolder}')
#     builtins.BB_DEVCACHEPATH = ARGS.cachefolder

# pylint: disable=C0412, C0413
from build_tools import (build_avatar_provider, create_default_avatars,
                         gen_locales)
gen_locales(force=ARGS.rebuild)

if ARGS.settings:
    from bubblemail.ui.settingswindow import SettingsWindow
    SettingsWindow()
else:
    build_avatar_provider(force=ARGS.rebuild)
    create_default_avatars(force=ARGS.rebuild)
    from bubblemail.service import Service
    Service().run()
