# Changelog
## [v0.8] -

## [v0.7] - 2020-04-14
 - Add folder selection button directly on account row (221f4542, d064bce1, ab3ac138, 7e77f1e6, 620985ea, d0f9ce11)
 - Show folders as a tree (d869ba7e, 33d1f163)
 - Decode Imap folders in native charset (2ad7c49e)
 - Add show account name option to libnotify plugin (59d40397)
 - Improve connexion errors handling (6ddd8664)
 - Improve developement mode providing home folder user's choice (eda180de)
 - Fix bug: non printable caracters must be allowed for passwords (14413593)
 - Fix bug: Undoing account delete leading to missing account settings (e88d4216)
 - Fix bug: crash loading gnome online account without credentials (2adc6c90)
 - Fix bug: New account not always added
 - Fix bug: Libnotify plugin is abnormaly changing mail list items (8b646e63)
 - Fix bug: folders selection on local maildir account not working (7e705572)

## [v0.6] - 2020-03-30
 - Remove version checker (packaging for all distribution is done, no need for it anymore) (10638011)
 - Stop account mail collect on first encoutered timeout (0209d0a8)
 - Add greek translation, credits to Alexander Ploumistos (8038e4c8)
 - Improve error handling (4c4c21ac, 2c50afcc, 3df11705, 896e8165)
 - Improve various email format support (3dcbbed7, 75dde255)
 - Improve reliability and exceptions handling (e810e932, 27e7bc98, bca84c50, 83e6ee46, 4d08cbaf, 812eccd1)
 - Allow spaces in account names (cc01f6b6)
 - Save config only on mutation (7da59d92)
 - Fix bug: adding pop3 account not working (1c6be89f)
 - Fix bug: Gtk interface not always refreshed (4bedd4a7)
 - Extend test units cases providing problematic raw emails (b719f0a8)

## [v0.5] - 2020-02-19
 - Libnotify plugin: Add per mail notification feature (2953720e)
 - Keep previous collect results on remote account failure (3acf4982)
 - Save and restore main window size (830c87ef, e857a923 and 358c8f26)
 - Implement first run auto discovering of local mail sources (040b241c)
 - Improve local mail support providing multiple local accounts (6c07190b)
 - Get better account creation/edition coherence avoiding backend changes (59d8652d, 72304c7e)
 - Get better data file tree coherence moving plugins ui files to data folder(79302ec5, 0f25f90d)
 - Improve legacy compatibility providing better appdata.xml content and path (8a97143c, a69f744e and 4b584a3f)
 - Produce debug output during vala build (3e25109a)
 - Bug fixes

## [v0.4] - 2020-01-15
 - Rename binaries and desktop files with simplicity in mind (a3d1d59e, 3694b860)
   - Service, formely `bubblemail-service` become `bubblemaild`
   - Settings, formely `bubblemail-settings` become `bubblemail`
 - Add new version checker using gitlab api, inform user for new releases via a dialog (4adfd984)
 - Avoid avatar generation error if libfolks not installed, checking for it's installation at first (f842675a 4adfd984)
 - Fix bug providing avatar file that doesn't exist anymore, revert to default ones in this case (f842675a)
 - Fix bug dismissed mails are forgotten in case of account connexion error between checks (051d364a)
 - Fix bug: undo after account deletion not working (62bd432f)
 - Fix bug: Locale po files outdated (ec0e5b08)
 - Various clean ups

## [v0.3] - 2019-12-16
 - Add default colorized avatars based on sender name or address (c9c6c09b bcec0a2c)
 - Improve remote mail error reporting (c61ad860)
 - Add insecure connection warning (023c925c)
 - Add reverse order option for libnotify plugin (c9c6c09b)
 - Add config version property & check for config file incompatibity (4ca83d37)
 - Add dbus method & signal providing config (406c8496)
   - Used by gs extension to check version compatibility and account error reporting
 - Use dbus string array signature everywere (a9fe72d1)
 - Use account uuid instead of account name for mail to account relationship (a9fe72d1)
 - Use xdg cache folder instead of config for logs and caches (10c0b4c0)
 - Use pickle for mail cache (10c0b4c0)
 - Fix bug: version not always checked (fdfa357e)
 - Various clean ups